#!/usr/bin/env python
# -*- coding: utf-8 -*-
from app import auth, nslxc, nslgw
from .views import *
from .views_datacenter import *
from .views_lxd import *

# Auth routes
auth.add_resource(Auth, '/auth')
auth.add_resource(AuthOtp, '/auth/otp')
auth.add_resource(AuthRefresh, '/auth/refresh')
auth.add_resource(AuthCheck, '/auth/check')
auth.add_resource(AuthLogout, '/auth/logout')

# Users routes
nslgw.add_resource(UsersList, '/users')
nslgw.add_resource(Users, '/users/<int:id>')
nslgw.add_resource(Me, '/me')
nslgw.add_resource(MeOtp, '/me/otp')
nslgw.add_resource(GroupsList, '/groups')
nslgw.add_resource(Groups, '/groups/<int:id>')
nslgw.add_resource(ProjectsList, '/projects')
nslgw.add_resource(Roles, '/roles/<int:id>')
nslgw.add_resource(RolesList, '/roles')
nslgw.add_resource(RoleAbilities, '/roleabilities/<int:id>')
nslgw.add_resource(RoleAbilitiesList, '/roleabilities')
nslgw.add_resource(Projects, '/projects/<int:id>')
nslgw.add_resource(AbilitiesList, '/abilities')
nslgw.add_resource(Abilities, '/abilities/<int:id>')
nslgw.add_resource(RequestsList, '/requests')
nslgw.add_resource(Requests, '/requests/<int:id>')
nslgw.add_resource(AllUserProjectsList, '/users/projects/')
nslgw.add_resource(UserProjectsList, '/users/<int:user_id>/projects/')
nslgw.add_resource(UserRoleProjectsList, '/projects/<int:project_id>/users/<int:user_id>/')
nslgw.add_resource(UserRoleProjects, '/projects/<int:project_id>/users/<int:user_id>/roles/<int:role_id>')

#datacenters and clusters routes
nslgw.add_resource(DataCentersList, '/datacenters')
nslgw.add_resource(DataCenters, '/datacenters/<int:id>')


nslgw.add_resource(DatacenterClustersList, '/datacenters/<int:datacenter_id>/clusters')
nslgw.add_resource(DatacenterClusterTypeClustersList, '/datacenters/<int:datacenter_id>/cluster_types/<int:cluster_type_id>/clusters')
nslgw.add_resource(ClustersList, '/clusters/')
nslgw.add_resource(Clusters, '/clusters/<int:id>')

nslgw.add_resource(ClusterTypesList, '/cluster_types')
nslgw.add_resource(ClusterTypes, '/cluster_types/<int:id>')

nslgw.add_resource(HostTypesList, '/host_types')
nslgw.add_resource(HostTypes, '/host_types/<int:id>')


nslgw.add_resource(ClusterHostsList, '/clusters/<int:cluster_id>/hosts')
nslgw.add_resource(ClusterHostTypeHostsList, '/clusters/<int:cluster_id>/host_types/<int:host_type_id>/hosts')
nslgw.add_resource(Hosts, '/hosts/<int:id>')

nslgw.add_resource(FlavorsList, '/flavors')
nslgw.add_resource(Flavors, '/flavors/<int:id>')

# nslgw.add_resource(ClusterFlavorHostTypesList, '/flavors')
nslgw.add_resource(ClusterFlavorHostTypes, '/clusters/<int:cluster_id>/flavors/<int:flavor_id>/host_types/<int:host_type_id>')

#network routes
nslgw.add_resource(ClusterNetworksList, '/clusters/<int:cluster_id>/networks')
nslgw.add_resource(NetworksList, '/networks')
nslgw.add_resource(Networks, '/networks/<int:id>')

nslgw.add_resource(NetworkNetworkSubnetsList, '/networks/<int:network_id>/network_subnets')
nslgw.add_resource(NetworkSubnetsList, '/network_subnets')
nslgw.add_resource(NetworkSubnets, '/network_subnets/<int:network_subnet_id>')

nslgw.add_resource(SubnetsNetworkIpsList, '/network_subnets/<int:network_subnet_id>/network_ips')
nslgw.add_resource(ContainerNetworkIpsList, '/container/<int:container_id>/network_ips')
nslgw.add_resource(ContainerNetworkIps, '/container/<int:container_id>/network_ips/<int:network_ip_id>/')
nslgw.add_resource(ProjectClusterNetworkIpsList, '/projects/<int:project_id>/clusters/<int:cluster_id>/network_ips')
nslgw.add_resource(HostNetworkIps, '/hosts/<int:host_id>/network_ips/<int:network_ip_id>/')
nslgw.add_resource(NetworkIpsList, '/network_ips/')
nslgw.add_resource(NetworkIps, '/network_ips/<int:id>')

##need to local port and external port
nslgw.add_resource(ContainerNetworkProxysList, '/container/<int:container_id>/network_proxys') #create
nslgw.add_resource(ShareNetworkIpsNetworkProxysList, '/share_network_ips/<int:network_ip_id>/network_proxys')
nslgw.add_resource(NetworkProxysList, '/network_proxys/')
nslgw.add_resource(NetworkProxys, '/network_proxys/<int:id>')


# Containers routes
nslxc.add_resource(ContainersList, '/projects/<int:project_id>/containers')
nslxc.add_resource(Containers, '/containers/<int:id>')
nslxc.add_resource(ContainersStart, '/containers/<int:id>/start')
nslxc.add_resource(ContainersFreeze, '/containers/<int:id>/freeze')
nslxc.add_resource(ContainersUnfreeze, '/containers/<int:id>/unfreeze')
nslxc.add_resource(ContainersStop, '/containers/<int:id>/stop')
nslxc.add_resource(ContainersRestart, '/containers/<int:id>/restart')
nslxc.add_resource(ContainersExec, '/containers/<int:id>/exec')
nslxc.add_resource(ContainersState, '/containers/<int:id>/state')

# Special and config routes
nslxc.add_resource(Operations, '/operations/<string:id>')
nslgw.add_resource(CtsStats, '/stats')
nslgw.add_resource(LXDConfig, '/lxdconfig')
nslgw.add_resource(LXDCerts, '/lxdcerts')
nslxc.add_resource(LxcHostResources, '/resources')
nslxc.add_resource(LxcCheckConfig, '/checkconfig')

# Snapshots routes
nslxc.add_resource(SnapshotsList, '/containers/<int:id>/snapshots')
nslxc.add_resource(Snapshots, '/containers/<int:id>/snapshots/<string:name>')
nslxc.add_resource(SnapshotsRestore, '/containers/<int:id>/snapshots/<string:name>/restore')

# Images routes
nslxc.add_resource(ImagesList, '/projects/<int:project_id>/images')
nslxc.add_resource(Images, '/images/<string:fingerprint>')
nslxc.add_resource(ImagesAliasesList, '/images/aliases')
nslxc.add_resource(ImagesAliases, '/images/aliases/<path:alias>')
nslxc.add_resource(RemoteImagesList, '/images/remote')
