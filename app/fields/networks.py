#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_networks_fields_attributes = api.model('NetworksFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
    'maintenance_mode': fields.Boolean,
    'external_network': fields.Boolean,
    'cloud_metadata': fields.String,
    'ipcost': fields.Integer,
    'portcost': fields.Integer,
    'tarfficcost': fields.Integer,
    'cluster': fields.Nested(api.model('ClusterFieldsAttributes', {'id': fields.Integer, 'name': fields.String}))

})


_networks_fields_attributes_put = api.inherit('NetworksFieldsAttributesPut',_networks_fields_attributes, {
    'name': fields.String( pattern='^(?!\s*$).+'),

})

_networks_fields_attributes_post = api.inherit('NetworksFieldsAttributesPost',_networks_fields_attributes, {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),


})

_networks_fields_attributes_get = api.inherit('NetworksFieldsAttributesGet',_networks_fields_attributes, {
    'cluster': fields.Nested(api.model('ClusterFieldsAttributes', {'id': fields.Integer, 'name': fields.String}))

})


_networks_fields_get_many = api.inherit('NetworksFieldsGetMany', _networks_fields_attributes_get, {
    'type': fields.String(default='networks'),
    'id': fields.Integer,
    'name': fields.String( pattern='^(?!\s*$).+'),
})



_networks_fields_get = api.inherit('NetworksFieldsGet', _networks_fields_attributes_get, {
    'type': fields.String(default='networks')
})

_networks_fields_post = api.inherit('NetworksFieldsPost', _networks_fields_attributes_post, {
    'type': fields.String(pattern='networks', default='networks')
})

_networks_fields_put= api.inherit('NetworksFieldsPut', _networks_fields_attributes_put, {
    'type': fields.String(pattern='networks', default='networks')
})


networks_fields_get = api.model('NetworksRootGet', { 'data': fields.Nested(_networks_fields_get) })
networks_fields_get_many = api.model('NetworksRootGetMany', { 'data': fields.Nested(_networks_fields_get_many, as_list=True) })
networks_fields_post = api.model('NetworksRootPost', { 'data': fields.Nested(_networks_fields_post) })
networks_fields_put=api.model('NetworksRootPut', { 'data': fields.Nested(_networks_fields_put) })