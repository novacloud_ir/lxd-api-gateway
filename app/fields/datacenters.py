#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_datacenters_fields_attributes = api.model('DatacentersFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
    'maintenance_mode': fields.Boolean,
    'cloud_metadata': fields.String,
})


_datacenters_fields_get_many = api.inherit('DatacentersFieldsGetMany', _datacenters_fields_attributes, {
    'type': fields.String(default='datacenters'),
    'id': fields.Integer,

})



_datacenters_fields_get = api.inherit('DatacentersFieldsGet', _datacenters_fields_attributes, {
    'type': fields.String(default='datacenters')
})

_datacenters_fields_post = api.inherit('DatacentersFieldsPost', _datacenters_fields_attributes, {
    'type': fields.String(pattern='datacenters', default='datacenters')
})

_datacenters_fields_put= api.inherit('DatacentersFieldsPut', _datacenters_fields_attributes, {
    'type': fields.String(pattern='datacenters', default='datacenters')
})


datacenters_fields_get = api.model('DatacentersRootGet', { 'data': fields.Nested(_datacenters_fields_get) })
datacenters_fields_get_many = api.model('DatacentersRootGetMany', { 'data': fields.Nested(_datacenters_fields_get_many, as_list=True) })
datacenters_fields_post = api.model('DatacentersRootPost', { 'data': fields.Nested(_datacenters_fields_post) })
datacenters_fields_put=api.model('DatacentersRootPut', { 'data': fields.Nested(_datacenters_fields_put) })