#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api



_user_roles_projects_fields_attributes= api.model('UserProjectsRolesFieldsAttributes', {
            'id': fields.Integer,
            'name': fields.String,
            'projects': fields.Nested(api.model('ProjectRolesAttributeData', {
                'id': fields.Integer,
                'name': fields.String,
                'Roles': fields.Nested(api.model('RolesDataAttribute', {
                    'id': fields.Integer,
                    'name': fields.String
                 }), as_list=True),
            }), as_list=True)
    })


_user_roles_projects_fields_relationships_get =  api.inherit('UserProjectsRolesRelationshipsGet',
            _user_roles_projects_fields_attributes,{
            'type': fields.String(default='users_projects_roles')
})

_users_roles_projects_fields_relationships_post_put =  api.inherit('UserProjectsRolesRelationshipsPost',
            _user_roles_projects_fields_attributes,{
            'type': fields.String(pattern='users_projects_roles', default='users_projects_roles')
})




users_roles_projects_fields_get = api.model('UsersRolesProjectsRootGet',
                        { 'data': fields.Nested(_user_roles_projects_fields_relationships_get) })
users_roles_projects_fields_get_many = api.model('UsersRolesProjectsRootGetMany',
                        { 'data': fields.Nested(_user_roles_projects_fields_relationships_get, as_list=True) })
# users_roles_projects_fields_post = api.model('UsersRolesProjectsRootPost',
#                         { 'data': fields.Nested(_users_roles_projects_fields_relationships_post_put) })
users_roles_projects_fields_put = api.model('UsersRolesProjectsRootPut',
                        { 'data': fields.Nested(_users_roles_projects_fields_relationships_post_put) })