#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api

_roles_fields_attributes_get = api.model('RolesFieldsAttributesGet', {
    'name': fields.String(required=True)

})


_roles_fields_attributes_put_post = api.model('RolesFieldsAttributesPutPost', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),

})


_roles_fields_get_many = api.model('RolesFieldsGetMany', {
    'type': fields.String(default='roles'),
    'id': fields.Integer,
    'name': fields.String
})



_roles_fields_get = api.inherit('RolesFieldsGet', _roles_fields_attributes_get, {
    'type': fields.String(default='roles')
})

_roles_fields_post = api.inherit('RolesFieldsPost', _roles_fields_attributes_put_post, {
    'type': fields.String(pattern='roles', default='roles')
})

_roles_fields_put= api.inherit('RolesFieldsPut', _roles_fields_attributes_put_post, {
    'type': fields.String(pattern='roles', default='roles')
})


roles_fields_get = api.model('RolesRootGet', { 'data': fields.Nested(_roles_fields_get) })
roles_fields_get_many = api.model('RolesRootGetMany', { 'data': fields.Nested(_roles_fields_get_many, as_list=True) })
roles_fields_post = api.model('RolesRootPost', { 'data': fields.Nested(_roles_fields_post) })
roles_fields_put=api.model('RolesRootPut', { 'data': fields.Nested(_roles_fields_put) })