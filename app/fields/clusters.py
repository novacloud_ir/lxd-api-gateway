#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api

_clusters_fields_attributes = api.model('ClustersFieldsAttributes', {
    'endpoint': fields.String,
    'maintenance_mode': fields.Boolean,
    'cpu_sum':  fields.Integer,  # unit => 1 core = 1000 number for example 8 core 8000
    'memory_sum':  fields.Integer,  # unit MB
    'disk_sum':  fields.Integer, # unit GB
    'cpu_aloc_sum': fields.Integer,
    'memory_aloc_sum':  fields.Integer,
    'disk_aloc_sum':  fields.Integer,

})

_clusters_fields_attributes_put_post=api.inherit('ClustersFieldsAttributesPutPost',_clusters_fields_attributes,{
     'name': fields.String(pattern='^(?!\s*$).+'),
     'lxd_metadata':  fields.Nested(api.model('ClusterLxdMetadataFieldsAttributes',
                                                {'cert_crt': fields.String,
                                                'cert_key': fields.String})),
})


_clusters_fields_attributes_put = api.inherit('ClustersFieldsAttributesPut',_clusters_fields_attributes_put_post,{
     'name': fields.String(pattern='^(?!\s*$).+'),
})

_clusters_fields_attributes_post = api.inherit('ClustersFieldsAttributesPost', _clusters_fields_attributes_put_post,{
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
})


_clusters_fields_attributes_get = api.inherit('ClustersFieldsAttributesGet',_clusters_fields_attributes, {
    'cluster_type':  fields.Nested(api.model('ClusterClusterTypesFieldsAttributes', {'id': fields.Integer, 'name': fields.String})),
    'datacenter': fields.Nested(api.model('ClusterDatacenterFieldsAttributes', {'id': fields.Integer, 'name': fields.String})),
    'cluster_metadata':fields.Raw,
})


_clusters_fields_get_many = api.inherit('ClustersFieldsGetMany', _clusters_fields_attributes_get, {
    'type': fields.String(default='clusters'),
    'id': fields.Integer,

})



_clusters_fields_get = api.inherit('ClustersFieldsGet', _clusters_fields_attributes_get, {
    'type': fields.String(default='clusters')
})

_clusters_fields_post = api.inherit('ClustersFieldsPost', _clusters_fields_attributes_post, {
    'type': fields.String(pattern='clusters', default='clusters'),

})

_clusters_fields_put= api.inherit('ClustersFieldsPut', _clusters_fields_attributes_put, {
    'type': fields.String(pattern='clusters', default='clusters'),
})


clusters_fields_get = api.model('ClustersRootGet', { 'data': fields.Nested(_clusters_fields_get) })
clusters_fields_get_many = api.model('ClustersRootGetMany', { 'data': fields.Nested(_clusters_fields_get_many, as_list=True) })
clusters_fields_post = api.model('ClustersRootPost', { 'data': fields.Nested(_clusters_fields_post) })
clusters_fields_put=api.model('ClustersRootPut', { 'data': fields.Nested(_clusters_fields_put) })