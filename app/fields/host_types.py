#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_host_types_fields_attributes = api.model('HostTypesFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
    'cloud_metadata': fields.String,
})


_host_types_fields_get_many = api.inherit('HostTypesFieldsGetMany', _host_types_fields_attributes, {
    'type': fields.String(default='host_types'),
    'id': fields.Integer,

})



_host_types_fields_get = api.inherit('HostTypesFieldsGet', _host_types_fields_attributes, {
    'type': fields.String(default='host_types')
})

_host_types_fields_post = api.inherit('HostTypesFieldsPost', _host_types_fields_attributes, {
    'type': fields.String(pattern='host_types', default='host_types')
})

_host_types_fields_put= api.inherit('HostTypesFieldsPut', _host_types_fields_attributes, {
    'type': fields.String(pattern='host_types', default='host_types')
})


host_types_fields_get = api.model('HostTypesRootGet', { 'data': fields.Nested(_host_types_fields_get) })
host_types_fields_get_many = api.model('HostTypesRootGetMany', { 'data': fields.Nested(_host_types_fields_get_many, as_list=True) })
host_types_fields_post = api.model('HostTypesRootPost', { 'data': fields.Nested(_host_types_fields_post) })
host_types_fields_put=api.model('HostTypesRootPut', { 'data': fields.Nested(_host_types_fields_put) })