#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_flavors_fields_attributes = api.model('FlavorsFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
    'cloud_metadata': fields.String,
    'cpu': fields.Integer,
    'memory': fields.Integer,
    'disk': fields.Integer,
    'traffic': fields.Integer,

})


_flavors_fields_get_many = api.inherit('FlavorsFieldsGetMany', _flavors_fields_attributes, {
    'type': fields.String(default='flavors'),
    'id': fields.Integer,

})



_flavors_fields_get = api.inherit('FlavorsFieldsGet', _flavors_fields_attributes, {
    'type': fields.String(default='flavors')
})

_flavors_fields_post = api.inherit('FlavorsFieldsPost', _flavors_fields_attributes, {
    'type': fields.String(pattern='flavors', default='flavors')
})

_flavors_fields_put= api.inherit('FlavorsFieldsPut', _flavors_fields_attributes, {
    'type': fields.String(pattern='flavors', default='flavors')
})


flavors_fields_get = api.model('FlavorsRootGet', { 'data': fields.Nested(_flavors_fields_get) })
flavors_fields_get_many = api.model('FlavorsRootGetMany', { 'data': fields.Nested(_flavors_fields_get_many, as_list=True) })
flavors_fields_post = api.model('FlavorsRootPost', { 'data': fields.Nested(_flavors_fields_post) })
flavors_fields_put=api.model('FlavorsRootPut', { 'data': fields.Nested(_flavors_fields_put) })