#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_role_abilities_fields_attributes = api.model('RoleAbilitiesFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
})


_role_abilities_fields_get_many = api.model('RoleAbilitiesFieldsGetMany', {
    'type': fields.String(default='role_abilities'),
    'id': fields.Integer,
    # 'uuid' : fields.String,
    'name': fields.String
})



_role_abilities_fields_get = api.inherit('RoleAbilitiesFieldsGet', _role_abilities_fields_attributes, {
    'type': fields.String(default='role_abilities')
})

_role_abilities_fields_post = api.inherit('RoleAbilitiesFieldsPost', _role_abilities_fields_attributes, {
    'type': fields.String(pattern='role_abilities', default='role_abilities')
})

_role_abilities_fields_put= api.inherit('RoleAbilitiesFieldsPut', _role_abilities_fields_attributes, {
    'type': fields.String(pattern='role_abilities', default='role_abilities')
})


role_abilities_fields_get = api.model('RoleAbilitiesRootGet', { 'data': fields.Nested(_role_abilities_fields_get) })
role_abilities_fields_get_many = api.model('RoleAbilitiesRootGetMany', { 'data': fields.Nested(_role_abilities_fields_get_many, as_list=True) })
role_abilities_fields_post = api.model('RoleAbilitiesRootPost', { 'data': fields.Nested(_role_abilities_fields_post) })
role_abilities_fields_put=api.model('RoleAbilitiesRootPut', { 'data': fields.Nested(_role_abilities_fields_put) })