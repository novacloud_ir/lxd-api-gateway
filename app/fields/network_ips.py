#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api



_network_ips_fields_container_project_attributes = api.model('NetworkIpsFieldsContainerProjectAttributes', {
    'cloud_metadata': fields.String,
})

_network_ips_fields_attributes = api.model('NetworkIpsFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
    'ip_address': fields.String,
    'port_sharing': fields.Boolean,
    'cloud_metadata': fields.String,
})

_network_ips_fields_attributes_relationship = api.inherit('NetworkIpsFieldsAttributeRelationships', _network_ips_fields_attributes, {

    'project': fields.Nested(
        api.model('NetworkSubnetFieldsAttributes', {'id': fields.Integer, 'name': fields.String})),
    'network_subnet': fields.Nested(
        api.model('NetworkSubnetFieldsAttributes', {'id': fields.Integer, 'name': fields.String})),
    'container': fields.Nested(
        api.model('NetworkSubnetFieldsAttributes', {'id': fields.Integer, 'name': fields.String})),
    'host': fields.Nested(
        api.model('NetworkSubnetFieldsAttributes', {'id': fields.Integer, 'name': fields.String})),

})



_network_ips_fields_get_many = api.inherit('NetworkIpsFieldsGetMany', _network_ips_fields_attributes_relationship, {
    'type': fields.String(default='network_ips'),
    'id': fields.Integer,

})



_network_ips_fields_get = api.inherit('NetworkIpsFieldsGet', _network_ips_fields_attributes_relationship, {
    'type': fields.String(default='network_ips')
})

_network_ips_fields_post = api.inherit('NetworkIpsFieldsPost', _network_ips_fields_attributes, {
    'type': fields.String(pattern='network_ips', default='network_ips')
})

_network_ips_fields_put= api.inherit('NetworkIpsFieldsPut', _network_ips_fields_attributes, {
    'type': fields.String(pattern='network_ips', default='network_ips')
})


network_ips_fields_get = api.model('NetworkIpsRootGet', { 'data': fields.Nested(_network_ips_fields_get) })
network_ips_fields_get_many = api.model('NetworkIpsRootGetMany', { 'data': fields.Nested(_network_ips_fields_get_many, as_list=True) })
network_ips_fields_post = api.model('NetworkIpsRootPost', { 'data': fields.Nested(_network_ips_fields_post) })
network_ips_fields_container_project_post = api.model('NetworkIpsRootContainerProjectPost',
                                    { 'data': fields.Nested(_network_ips_fields_container_project_attributes) })
network_ips_fields_put=api.model('NetworkIpsRootPut', { 'data': fields.Nested(_network_ips_fields_put) })