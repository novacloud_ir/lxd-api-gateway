#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_projects_fields_attributes = api.model('ProjectsFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
    'cpuQuota': fields.Integer,
    'memQuota':fields.Integer,
    'diskQuota': fields.Integer,
    'trafficQuota':fields.Integer,
    'ipQuota':fields.Integer,
    'portQuota':fields.Integer,
    'cpuQuota_alloc': fields.Integer,
    'memQuota_alloc':fields.Integer,
    'diskQuota_alloc': fields.Integer,
    'trafficQuota_alloc':fields.Integer,
    'ipQuota_alloc':fields.Integer,
    'portQuota_alloc':fields.Integer,
})


_projects_fields_get_many = api.model('ProjectsFieldsGetMany', {
    'type': fields.String(default='projects'),
    'id': fields.Integer,
    'uuid' : fields.String,
    'name': fields.String
})



_projects_fields_get = api.inherit('ProjectsFieldsGet', _projects_fields_attributes, {
    'type': fields.String(default='projects')
})

_projects_fields_post = api.inherit('ProjectsFieldsPost', _projects_fields_attributes, {
    'type': fields.String(pattern='projects', default='projects')
})

_projects_fields_put= api.inherit('ProjectsFieldsPut', _projects_fields_attributes, {
    'type': fields.String(pattern='projects', default='projects')
})


projects_fields_get = api.model('ProjectsRootGet', { 'data': fields.Nested(_projects_fields_get) })
projects_fields_get_many = api.model('ProjectsRootGetMany', { 'data': fields.Nested(_projects_fields_get_many, as_list=True) })
projects_fields_post = api.model('ProjectsRootPost', { 'data': fields.Nested(_projects_fields_post) })
projects_fields_put=api.model('ProjectsRootPut', { 'data': fields.Nested(_projects_fields_put) })