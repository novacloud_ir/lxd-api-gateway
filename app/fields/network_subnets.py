#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_network_subnets_fields_attributes = api.model('NetworkSubnetsFieldsAttributes', {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),
    'cidr': fields.String,
    'maintenance_mode': fields.Boolean,
    'cloud_metadata': fields.String,
    'allocation_pools': fields.String,
    'dns_nameservers': fields.String,
    'enable_dhcp': fields.String,
    'gateway_ip': fields.String,
    'network': fields.Nested(api.model('NetworkFieldsAttributes', {'id': fields.Integer, 'name': fields.String}))

})


_network_subnets_fields_get_many = api.inherit('NetworkSubnetsFieldsGetMany', _network_subnets_fields_attributes, {
    'type': fields.String(default='network_subnets'),
    'id': fields.Integer,

})



_network_subnets_fields_get = api.inherit('NetworkSubnetsFieldsGet', _network_subnets_fields_attributes, {
    'type': fields.String(default='network_subnets')
})

_network_subnets_fields_post = api.inherit('NetworkSubnetsFieldsPost', _network_subnets_fields_attributes, {
    'type': fields.String(pattern='network_subnets', default='network_subnets')
})

_network_subnets_fields_put= api.inherit('NetworkSubnetsFieldsPut', _network_subnets_fields_attributes, {
    'type': fields.String(pattern='network_subnets', default='network_subnets')
})


network_subnets_fields_get = api.model('NetworkSubnetsRootGet', { 'data': fields.Nested(_network_subnets_fields_get) })
network_subnets_fields_get_many = api.model('NetworkSubnetsRootGetMany', { 'data': fields.Nested(_network_subnets_fields_get_many, as_list=True) })
network_subnets_fields_post = api.model('NetworkSubnetsRootPost', { 'data': fields.Nested(_network_subnets_fields_post) })
network_subnets_fields_put=api.model('NetworkSubnetsRootPut', { 'data': fields.Nested(_network_subnets_fields_put) })