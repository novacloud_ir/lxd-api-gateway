#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api





_hosts_fields_attributes = api.model('HostsFieldsAttributes', {
    'maintenance_mode': fields.Boolean,
    'cloud_metadata': fields.String,
    'cpu':  fields.Integer,  # unit => 1 core = 1000 number for example 8 core 8000
    'memory':  fields.Integer,  # unit MB
    'disk':  fields.Integer, # unit GB
    'cpu_share': fields.Integer,
    'memory_share':  fields.Integer,
    'cpu_aloc':  fields.Integer,
    'memory_aloc':  fields.Integer,
    'disk_aloc':  fields.Integer,

})

_hosts_fields_attributes_put = api.inherit('HostsFieldsAttributesPut',_hosts_fields_attributes, {
    'name': fields.String( pattern='^(?!\s*$).+'),

})

_hosts_fields_attributes_post = api.inherit('HostsFieldsAttributesPost', _hosts_fields_attributes, {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),

})

_hosts_fields_attributes_get = api.inherit('HostsFieldsAttributesGet', _hosts_fields_attributes, {
    'name': fields.String(pattern='^(?!\s*$).+'),
    'host_type':  fields.Nested(api.model('HostTypesFieldsAttributes', {'id': fields.Integer, 'name': fields.String})),
    'cluster': fields.Nested(api.model('ClusterFieldsAttributes', {'id': fields.Integer, 'name': fields.String}))

})

_hosts_fields_get_many = api.inherit('HostsFieldsGetMany', _hosts_fields_attributes_get, {
    'type': fields.String(default='hosts'),
    'id': fields.Integer,

})



_hosts_fields_get = api.inherit('HostsFieldsGet', _hosts_fields_attributes_get, {
    'type': fields.String(default='hosts')
})

_hosts_fields_post = api.inherit('HostsFieldsPost', _hosts_fields_attributes_post, {
    'type': fields.String(pattern='hosts', default='hosts')
})

_hosts_fields_put= api.inherit('HostsFieldsPut', _hosts_fields_attributes_put, {
    'type': fields.String(pattern='hosts', default='hosts')
})


hosts_fields_get = api.model('HostsRootGet', { 'data': fields.Nested(_hosts_fields_get) })
hosts_fields_get_many = api.model('HostsRootGetMany', { 'data': fields.Nested(_hosts_fields_get_many, as_list=True) })
hosts_fields_post = api.model('HostsRootPost', { 'data': fields.Nested(_hosts_fields_post) })
hosts_fields_put=api.model('HostsRootPut', { 'data': fields.Nested(_hosts_fields_put) })