#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask_restplus import fields
from app import api



_cluster_types_fields_attributes_put = api.model('ClusterTypesFieldsAttributesPut', {
    'cluster_metadata_template': fields.Raw,
    'cloud_metadata': fields.Raw,
})

_cluster_types_fields_attributes = api.inherit('ClusterTypesFieldsAttributes',_cluster_types_fields_attributes_put, {
    'name': fields.String(required=True, pattern='^(?!\s*$).+'),

})


_cluster_types_fields_get_many = api.inherit('ClusterTypesFieldsGetMany', _cluster_types_fields_attributes, {
    'type': fields.String(default='cluster_types'),
    'id': fields.Integer,

})



_cluster_types_fields_get = api.inherit('ClusterTypesFieldsGet', _cluster_types_fields_attributes, {
    'type': fields.String(default='cluster_types')
})

_cluster_types_fields_post = api.inherit('ClusterTypesFieldsPost', _cluster_types_fields_attributes, {
    'type': fields.String(pattern='cluster_types', default='cluster_types')
})

_cluster_types_fields_put= api.inherit('ClusterTypesFieldsPut', _cluster_types_fields_attributes_put, {
    'type': fields.String(pattern='cluster_types', default='cluster_types')
})


cluster_types_fields_get = api.model('ClusterTypesRootGet', { 'data': fields.Nested(_cluster_types_fields_get) })
cluster_types_fields_get_many = api.model('ClusterTypesRootGetMany', { 'data': fields.Nested(_cluster_types_fields_get_many, as_list=True) })
cluster_types_fields_post = api.model('ClusterTypesRootPost', { 'data': fields.Nested(_cluster_types_fields_post) })
cluster_types_fields_put=api.model('ClusterTypesRootPut', { 'data': fields.Nested(_cluster_types_fields_put) })