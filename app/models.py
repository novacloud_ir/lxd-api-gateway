#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import db, redis_store
from app.exceptions import *
from sqlalchemy import and_
from passlib.apps import custom_app_context as pwd_context
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.dialects.postgresql import UUID

from netaddr import IPAddress,IPRange
import datetime
import os
import base64
import onetimepass
import configparser
from lgw import get_cert_path

def _user_find(u):
    user = User.query.get(u)
    if not(user):
        raise UserDoesntExist(u)
    return user

def _project_find(t):
    project = Project.query.get(t)
    if not(project):
        raise ProjectDoesntExist(t)
    return project


def _group_find(g):
    group = Group.query.get(g)
    if not(group):
        raise GroupDoesntExist(g)
    return group


def _ability_find(a):
    ability = Ability.query.get(a)
    if not(ability):
        raise AbilityDoesntExist(a)
    return ability

def _role_find(r):
    role = Role.query.get(r)
    if not(role):
        raise RoleDoesntExist(r)
    return role

def _role_abillity_find(r):
    role_abillity = RoleAbility.query.get(r)
    if not(role_abillity):
        raise RoleAbilityDoesntExist(r)
    return role_abillity


def _ability_find(a):
    ability = Ability.query.get(a)
    if not(ability):
        raise AbilityDoesntExist(a)
    return ability


def _container_find(c):
    container = Container.query.get(c)
    if not(container):
        raise ContainerDoesntExist(c)
    return container

def _container_type_find(c):
    container_type = ContainerType.query.get(c)
    if not(container_type):
        raise ContainerTypeDoesntExist(c)
    return container_type

def _request_find(r):
    request = Request.query.get(r)
    if not(request):
        raise RequestDoesntExist(r)
    return request

def _datacenter_find(d):
    datacenter = DataCenter.query.get(d)
    if not(datacenter):
        raise DataCenterDoesntExist(d)
    return datacenter

def _cluster_find(c):
    cluster = Cluster.query.get(c)
    if not (cluster):
        raise ClusterDoesntExist(c)
    return cluster

def _cluster_flavor_host_type_find(g):
    cluster_flavor_host_type = ClusterFlavorHostType.query.get(g)
    if not(cluster_flavor_host_type):
        raise ClusterFlavorHostTypeDoesntExist(g)
    return cluster_flavor_host_type

def _host_find(h):
    host = Cluster.query.get(h)
    if not (host):
        raise HostDoesntExist(h)
    return host

def _host_types_find(h):
    host_type = HostType.query.get(h)
    if not (host_type):
        raise HostTypeDoesntExist(h)
    return host_type

def _cluster_types_find(h):
    cluster_type = ClusterType.query.get(h)
    if not (cluster_type):
        raise ClusterTypeDoesntExist(h)
    return cluster_type

def _flavor_find(h):
    flavor = Flavor.query.get(h)
    if not (flavor):
        raise FlavorDoesntExist(h)
    return flavor

def _network_find(h):
    network = Network.query.get(h)
    if not (network):
        raise NetworkDoesntExist(h)
    return network

def _ip_allocation_pool_find(h):
    ip_allocation_pool = IPAllocationPool.query.get(h)
    if not (ip_allocation_pool):
        raise IPAllocationPoolDoesntExist(h)
    return ip_allocation_pool

def _network_subnet_find(h):
    network_subnet = NetworkSubnet.query.get(h)
    if not (network_subnet):
        raise NetworkSubnetDoesntExist(h)
    return network_subnet

def _network_ip_find(h):
    network_ip = NetworkIp.query.get(h)
    if not (network_ip):
        raise NetworkIpDoesntExist(h)
    return network_ip


user_group_table = db.Table(
    'user_group',
    db.Column(
        'user_id',
        db.Integer,
        db.ForeignKey('users.id')
    ),
    db.Column(
        'group_id',
        db.Integer,
        db.ForeignKey('groups.id')
    )
)



group_ability_table = db.Table(
    'group_ability',
    db.Column(
        'group_id',
        db.Integer,
        db.ForeignKey('groups.id')
    ),
    db.Column(
        'ability_id',
        db.Integer,
        db.ForeignKey('abilities.id')
    )
)

role_role_abilities_table=db.Table(
    'role_roleAbility',
    db.Column(
        'role_id',
        db.Integer,
        db.ForeignKey('roles.id')
    ),
    db.Column(
        'roleAbility_id',
        db.Integer,
        db.ForeignKey('role_abilities.id')
    )
)

user_request_table = db.Table(
    'user_request',
    db.Column(
        'user_id',
        db.Integer,
        db.ForeignKey('users.id')
    ),
    db.Column(
        'request_id',
        db.Integer,
        db.ForeignKey('requests.id')
    )
)






class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    admin = db.Column(db.Boolean, default=False, nullable=False)
    name = db.Column(db.String(255))
    username = db.Column(db.String(60), unique=True, nullable=False)
    registered_on = db.Column(db.DateTime, nullable=False)
    email = db.Column(db.String(120))
    phone = db.Column(db.String(255))
    address = db.Column(db.String(255))
    city = db.Column(db.String(120))
    country = db.Column(db.String(120))
    postal_code = db.Column(db.String(20))
    ico = db.Column(db.String(10))
    ic_dph = db.Column(db.String(10))
    dic = db.Column(db.String(10))
    password = db.Column(db.String(100), nullable=False)
    otp_type = db.Column(db.String(10))
    otp_secret = db.Column(db.String(20))
    language = db.Column(db.String(20), default='English')

    _groups = db.relationship(
        'Group',
        secondary=user_group_table
    )
    groups = association_proxy(
        '_groups',
        'id',
        creator=_group_find
    )

    _requests = db.relationship(
        'Request',
        secondary=user_request_table,
    )
    requests = association_proxy(
        '_requests',
        'id',
        creator=_request_find
    )

    def __init__(
        self,
        admin=False,
        name=None,
        username=None,
        registered_on=None,
        email=None,
        phone=None,
        address=None,
        city=None,
        country=None,
        postal_code=None,
        ico=None,
        ic_dph=None,
        dic=None,
        password=None,
        otp_type=None,
        otp_secret=None,
        language=None,
        groups=None,
        requests=None
    ):

        self.admin = admin
        self.name = name
        self.username = username
        self.registered_on = datetime.datetime.now()
        self.email = email
        self.phone = phone
        self.address = address
        self.city = city
        self.country = country
        self.postal_code = postal_code
        self.ico = ico
        self.ic_dph = ic_dph
        self.dic = dic
        self.password = password
        self.otp_type = otp_type
        self.otp_secret = otp_secret
        self.language = language

        if groups and isinstance(groups, list):
            self.groups = [group for group in groups]
        elif groups and isinstance(groups, int):
            self.groups = [groups]

        if requests and isinstance(requests, list):
            self.requests = [req for req in requests]
        elif requests and isinstance(requests, int):
            self.requests = [requests]

    def hash_password(self, password):
        self.password = pwd_context.hash(password)

    def verify_password(self, password):
        return pwd_context.verify(password, self.password)

    def add_totp_secret(self):
        if self.otp_secret is None:
            # generate a random secret
            self.otp_secret = base64.b32encode(os.urandom(10)).decode('utf-8')

    def get_totp_uri(self):
        config = configparser.ConfigParser()
        production_name = 'lxdmanager.com'
        try:
            config.read('lxdconfig.conf')
            production_name = config['app']['production_name']
        finally:
            return 'otpauth://totp/2FA-{0}:{1}?secret={2}&issuer=2FA-{0}' \
                .format(production_name, self.username, self.otp_secret)

    def verify_totp(self, token):
        return onetimepass.valid_totp(token, self.otp_secret)

    def has_totp_enabled(self):
        if self.otp_secret is None:
            return False
        else:
            return True

    def get_otp_type(self):
        return self.otp_type

    def create_eotp(self):
        #if self.otp_type == 'email':
        secret = base64.b32encode(os.urandom(10)).decode('utf-8')[2:8]
        s_hash = pwd_context.hash(secret)
        redis_store.set('eotp:' + self.username, s_hash, 300)
        return secret

    def verify_eotp(self, secret):
        r_hash = redis_store.get('eotp:' + self.username)
        redis_store.delete('eotp:' + self.username)
        return pwd_context.verify(secret, r_hash)



    def __jsonapi__(self, info=None):
        _json = {
            'type': 'users',
            'id': self.id,
            'admin': self.admin,
            'name': self.name,
            'username': self.username,
            'registered_on': self.registered_on,
            'email': self.email,
            'phone': self.phone,
            'address': self.address,
            'city': self.city,
            'country': self.country,
            'postal_code': self.postal_code,
            'ico': self.ico,
            'ic_dph': self.ic_dph,
            'dic': self.dic,
            'language': self.language,
            'otp_enabled': self.has_totp_enabled(),
            'otp_type': self.otp_type
        }

        if info == 'flat':
            return _json

        _json['relationships'] = {}


        _json['relationships']['requests'] = [
            req.__jsonapi__('flat') for req in self._requests]

        return _json

    def __repr__(self):
        return '<User %r>' % self.id


class Group(db.Model):
    __tablename__ = 'groups'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)

    _users = db.relationship(
        'User',
        secondary=user_group_table,
    )
    users = association_proxy(
        '_users',
        'id',
        creator=_user_find
    )

    _abilities = db.relationship(
        'Ability',
        secondary=group_ability_table
    )
    abilities = association_proxy(
        '_abilities',
        'id',
        creator=_ability_find
    )



    def __init__(
        self,
        name=None,
        abilities=None,
        # users=None
    ):

        self.name = name

        if abilities and isinstance(abilities, list):
            self.abilities = [ability for ability in abilities]
        elif abilities and isinstance(abilities, int):
            self.abilities = [abilities]

        # if users and isinstance(users, list):
        #     self.users = [user for user in users]
        # elif users and isinstance(users, int):
        #     self.users = [users]

    def __jsonapi__(self, group=None):
        _json = {
            'type': 'groups',
            'id': self.id,
            'name': self.name
        }

        if group == 'flat':
            return _json

        _json['relationships'] = {}
        _json['relationships']['abilities'] = {}
        #_json['relationships']['users'] = {}

        _json['relationships']['abilities'] = [
            ability.__jsonapi__('flat') for ability in self._abilities]
        # _json['relationships']['users'] = [
        #     user.__jsonapi__('flat') for user in self._users]

        return _json

    def __repr__(self):
        return '<Group %r>' % self.id

class Project(db.Model):
    __tablename__ = 'projects'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)

    cloud_metadata = db.Column(db.JSON)
    cpuQuota=db.Column(db.Integer)
    memQuota=db.Column(db.Integer)
    diskQuota=db.Column(db.Integer)
    trafficQuota=db.Column(db.Integer)
    ipQuota=db.Column(db.Integer)
    portQuota=db.Column(db.Integer)

    cpuQuota_alloc = db.Column(db.Integer)
    memQuota_alloc = db.Column(db.Integer)
    diskQuota_alloc = db.Column(db.Integer)
    trafficQuota_alloc = db.Column(db.Integer)
    ipQuota_alloc = db.Column(db.Integer)
    portQuota_alloc = db.Column(db.Integer)


    def __init__(
        self,
        name=None,
        cloud_metadata=None,
        cpu_quota=0,
        mem_quota=0,
        disk_quota=0,
        traffic_quota=0,
        ip_quota=0,
        port_quota=0,
        cpu_quota_alloc=0,
        mem_quota_alloc=0,
        disk_quota_alloc=0,
        traffic_quota_alloc=0,
        ip_quota_alloc=0,
        port_quota_alloc=0
    ):

        self.name = name
        self.cloud_metadata = cloud_metadata
        self.cpuQuota = cpu_quota
        self.memQuota = mem_quota
        self.diskQuota = disk_quota
        self.trafficQuota = traffic_quota
        self.ipQuota = ip_quota
        self.portQuota = port_quota
        self.cpuQuota_alloc = cpu_quota_alloc
        self.memQuota_alloc = mem_quota_alloc
        self.diskQuota_alloc = disk_quota_alloc
        self.trafficQuota_alloc = traffic_quota_alloc
        self.ipQuota_alloc = ip_quota_alloc
        self.portQuota_alloc = port_quota_alloc


    def add_containers(self, *containers):
        self.containers.extend(
            [container for container in containers if container not in self.containers])

    def remove_containers(self, *containers):
        self.containers = [
            container for container in self.containers if container not in containers]


    def __jsonapi__(self, info=None):
        _json = {
            'type': 'projects',
            'id': self.id,
            # 'uuid': self.uuid,
            'name': self.name,
            'cpuQuota': self.cpuQuota,
            'memQuota':self.memQuota,
            'diskQuota': self.diskQuota,
            'trafficQuota':self.trafficQuota,
            'ipQuota':self.ipQuota,
            'portQuota':self.portQuota,
            'cpuQuota_alloc': self.cpuQuota_alloc,
            'memQuota_alloc': self.memQuota_alloc,
            'diskQuota_alloc': self.diskQuota_alloc,
            'trafficQuota_alloc': self.trafficQuota_alloc,
            'ipQuota_alloc': self.ipQuota_alloc,
            'portQuota_alloc': self.portQuota_alloc,
            'cloud_metadata': self.cloud_metadata

        }

        # if info == 'flat':
        #     return _json
        #
        # _json['relationships'] = {}
        #
        # _json['relationships']['containers'] = {}
        #
        # _json['relationships']['containers'] = [
        #     container.__jsonapi__('flat') for container in self._containers]



        return _json

    def __repr__(self):
        return '<Project %r>' % self.id

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    # uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)

    cloud_metadata = db.Column(db.JSON)

    _role_abilities = db.relationship(
        'RoleAbility',
        secondary=role_role_abilities_table
    )
    role_abilities = association_proxy(
        '_role_abilities',
        'id',
        creator=_role_abillity_find
    )


    def __init__(
        self,
        name=None,
        cloud_metadata=None,
        role_abilities=None,
    ):

        self.name = name
        self.cloud_metadata = cloud_metadata

        if role_abilities and isinstance(role_abilities, list):
            self.role_abilities = [role_ability.id for role_ability in role_abilities]
        elif role_abilities and isinstance(role_abilities, int):
            self.role_abilities = [role_abilities.id]


    def __jsonapi__(self, info=None):
        _json = {
            'type': 'roles',
            'id': self.id,
            'name': self.name,
            'cloud_metadata': self.cloud_metadata

        }
        return _json

    def __repr__(self):
        return '<Role %r>' % self.id


class UserRoleProject(db.Model):
    __tablename__ = 'userRoleProjects'
    id = db.Column(db.Integer, primary_key=True)

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)


    db.UniqueConstraint('project_id', 'role_id',"user_id")
    db.relationship('Project', backref='userRoleProjects', lazy='dynamic')
    db.relationship('Role', backref='userRoleProjects', lazy='dynamic')
    db.relationship('user',  backref='userRoleProjects', lazy='dynamic')



    def __init__(
            self,
            project_id,
            user_id,
            role_id):

        self.project_id=project_id
        self.user_id=user_id
        self.role_id=role_id

    def get_role_abilities(self,user_id,project_id):
        """
        this function get user id and project id and return role ability
        :param user_id: int parameter
        :param project_id: int paramater
        :return:
        """
        role_abilities = []
        if user_id and isinstance(user_id, int) and project_id and isinstance(project_id, int) :
            user_role_projects = UserRoleProject.query \
                .filter(and_(UserRoleProject.user_id == user_id,
                             UserRoleProject.project_id == project_id
                             )).all()

            for user_role_project in user_role_projects:
                role = Role.query.get(user_role_project.role_id)
                role_abilities += role.role_abilities
        return role_abilities


    def user_project_role_json(self, user, _json=None, project=None):

        if _json is None:
            _json = {}
            _json['type'] = 'userRoleProjects'
            _json['user'] = {}
        _json['user']['id'] = user.id
        _json['user']['name'] = user.name
        if project :
            projects=project
        else:
            projects= [Project.quary.get(project_id).__jsonapi__('flat')
                            for project_id in UserRoleProject.quary(self.project_id).filter(self.user_id == user.id).all()]

        for project in projects:
            _json['user']['project'] = {}
            _json['user']['project']['id'] = project.id
            _json['user']['project']['name'] = project.name
            _json['user']['project']['roles'] = [
                Group.quary.get(role_id).__jsonapi__() for role_id in
                self.quary(self.role_id)
                    .filter(and_(self.user_id == user.id, self.project_id == project.id)).all()]
        return _json


    def __jsonapi__(self,user_id=None):
        _json = { }

        if user_id:
            user=User.quary.get(user_id)
            _json=self.user_project_role_json(user=user, _json=_json)
        else:
            for user in User.quary.all():
                _json=self.user_project_role_json(user, _json=_json)

        return _json


    def __jsonapi__(self,project, user):
        _json = self.user_project_role_json(user=user, project=project)
        return _json


    def __repr__(self):
        return '<UserRoleProject %r>' % self.id


class Ability(db.Model):
    __tablename__ = 'abilities'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True)
    _groups = db.relationship(
        'Group',
        secondary=group_ability_table
    )
    groups = association_proxy(
        '_groups',
        'id',
        creator=_group_find
    )

    def __init__(
        self,
        name=None,
        groups=None
    ):

        self.name = name

        if groups and isinstance(groups, list):
            self.groups = [group for group in groups]
        elif groups and isinstance(groups, int):
            self.groups = [groups]

    def __jsonapi__(self, group=None):
        _json = {
            'type': 'abilities',
            'id': self.id,
            'name': self.name
        }

        if group == 'flat':
            return _json

        _json['relationships'] = {}
        #_json['relationships']['groups'] = {}

        _json['relationships']['groups'] = [
            group.__jsonapi__('flat') for group in self._groups]

        return _json

    def __repr__(self):
        return '<Ability %r>' % self.id


class RoleAbility(db.Model):
    __tablename__ = 'role_abilities'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    # uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cloud_metadata = db.Column(db.JSON)

    _roles = db.relationship(
        'Role',
        secondary=role_role_abilities_table
    )
    roles = association_proxy(
        '_roles',
        'id',
        creator=_role_find
    )


    def __init__(
            self,
            name=None,
            cloud_metadata=None,

    ):
        self.name = name
        self.cloud_metadata = cloud_metadata




    def __jsonapi__(self, info=None):
        _json = {
            'type': 'role_abilities',
            'id': self.id,
            'name': self.name,
            'cloud_metadata': self.cloud_metadata

        }

    def __repr__(self):
        return '<Role %r>' % self.id


class Container(db.Model):
    __tablename__ = 'containers'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True)
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cloud_metadata = db.Column(db.JSON)

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=False)
    db.relationship('Projects', uselist=False, backref='containers', lazy='dynamic')

    cluster_flavor_host_type_id = db.Column(db.Integer, db.ForeignKey('cluster_flavor_host_types.id'), nullable=False)
    db.relationship('ClusterFlavorHostType', uselist=False, backref='containers', lazy='dynamic')

    # container_type_id = db.Column(db.Integer, db.ForeignKey('containerTypes.id'), nullable=False)
    # db.relationship('ContainerType', uselist=False, backref='containers', lazy='dynamic')

    host_id = db.Column(db.Integer, db.ForeignKey('hosts.id'), nullable=False)
    db.relationship('Host', uselist=False, backref='containers', lazy='dynamic')

    def __init__(
        self,
        name,
        project,
        cluster_flavor_host_type,
        # container_type,
        host,
        cloud_metadata=None,
    ):

        self.name = name
        self.cloud_metadata = cloud_metadata

        if project and isinstance(project, Project):
            self.project_id = project.id
        elif project and isinstance(project, int):
            self.project_id = project

        if cluster_flavor_host_type and isinstance(cluster_flavor_host_type, ClusterFlavorHostType):
            self.cluster_flavor_host_type_id = cluster_flavor_host_type.id
        elif cluster_flavor_host_type and isinstance(cluster_flavor_host_type, int):
            self.cluster_flavor_host_type_id = cluster_flavor_host_type

        # if container_type and isinstance(container_type, ContainerType):
        #     self.container_type_id = container_type.id
        # elif container_type and isinstance(container_type, int):
        #     self.container_type_id = container_type

        if host and isinstance(host, Host):
            self.host_id = host.id
        elif host and isinstance(host, int):
            self.host_id = host

    def __jsonapi__(self, info=None):
        project=_project_find(self.project_id)
        host=_host_find(self.host_id)
        cluster_flavor_host_type=_cluster_flavor_host_type_find(self.container_type_id)
        container_type = _container_type_find(self.container_type_id)
        _json = {
            'type': 'containers',
            'id': self.id,
            'name': self.name,
            'cloud_metadata': self.cloud_metadata,
            'project': {'id': project.id, 'name': project.name},
            'host': {'id': host.id, 'name': host.name},
            'cluster_flavor_host_type': {'id': cluster_flavor_host_type.id, 'name': cluster_flavor_host_type.name},
            # 'container_type': {'id': container_type.id, 'name': container_type.name},
        }

        return _json


    def __repr__(self):
        return '<Container %r>' % self.id


# 
# class ContainerType(db.Model):
#     __tablename__ = 'containerTypes'
#     id = db.Column(db.Integer, primary_key=True)
#     name = db.Column(db.String(255), unique=True)
#     #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)


class Request(db.Model):
    __tablename__ = 'requests'
    id = db.Column(db.Integer, primary_key=True)
    action = db.Column(db.String(255), unique=False)
    message = db.Column(db.String(255), unique=False)
    meta_data = db.Column(db.JSON, unique=False)
    status = db.Column(db.String(255), unique=False)
    created_on = db.Column(db.DateTime, nullable=False)
    changed_on = db.Column(db.DateTime)

    _users = db.relationship(
        'User',
        secondary=user_request_table,
    )
    users = association_proxy(
        '_users',
        'id',
        creator=_user_find
    )

    def __init__(
        self,
        action=None,
        message=None,
        meta_data=None,
        status=None,
        created_on=None,
        changed_on=None,
        users=None
    ):

        self.action = action
        self.message = message
        self.meta_data = meta_data
        self.status = status
        self.created_on = datetime.datetime.now()
        self.changed_on = changed_on

        if users and isinstance(users, list):
            self.users = [user for user in users]
        elif users and isinstance(users, int):
            self.users = [users]

    def __jsonapi__(self, group=None):
        _json = {
            'type': 'requests',
            'id': self.id,
            'action': self.action,
            'message': self.message,
            'meta_data': self.meta_data,
            'status': self.status,
            'created_on': self.created_on,
            'changed_on': self.changed_on
        }

        if group == 'flat':
            return _json

        _json['relationships'] = {}
        #_json['relationships']['users'] = {}

        _json['relationships']['users'] = [
            user.__jsonapi__('flat') for user in self._users]

        return _json

    def __repr__(self):
        return '<Request %r>' % self.id


class DataCenter(db.Model):
    __tablename__ = 'datacenters'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    maintenance_mode= db.Column(db.Boolean)
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cloud_metadata = db.Column(db.JSON)

    def __init__(
            self,
            name,
            maintenance_mode,
            cloud_metadata=None,

    ):
        self.name = name
        self.cloud_metadata = cloud_metadata
        self.maintenance_mode=maintenance_mode

    def __jsonapi__(self, info=None):
        _json = {
            'type': 'datacenters',
            'id': self.id,
            'name': self.name,
            'maintenance_mode': self.maintenance_mode,
            'cloud_metadata': self.cloud_metadata
        }
        return _json

    def __repr__(self):
        return '<DataCenter %r>' % self.id


class Cluster(db.Model):
    __tablename__ = 'clusters'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    endpoint=db.Column(db.String(255))
    cluster_metadata= db.Column(db.JSON)
    cloud_metadata = db.Column(db.JSON)
    maintenance_mode = db.Column(db.Boolean)

    cpu_sum = db.Column(db.BigInteger)  # unit => 1 core = 1000 number for example 8 core 8000
    memory_sum = db.Column(db.BigInteger)  # unit MB
    disk_sum = db.Column(db.BigInteger)  # unit GB

    cpu_aloc_sum  = db.Column(db.Integer)
    memory_aloc_sum  = db.Column(db.Integer)
    disk_aloc_sum  = db.Column(db.Integer)

    cluster_type_id = db.Column(db.Integer, db.ForeignKey('cluster_types.id'), nullable=False)
    db.relationship('ClusterTypes', uselist=False, backref='containers', lazy='dynamic')

    datacenter_id = db.Column(db.Integer, db.ForeignKey('datacenters.id'), nullable=False)
    db.relationship('DataCenter', uselist=False, backref='containers', lazy='dynamic')

    def __init__(
            self,
            name,
            datacenter,
            cluster_type,
            endpoint=None,
            cluster_metadata=None,
            maintenance_mode=True,
            cloud_metadata=None,
            cpu_sum=None,  # unit => 1 core = 1000 number for example 8 core 8000
            memory_sum=None,  # unit MB
            disk_sum=None,  # unit GB
            cpu_aloc_sum=None,
            memory_aloc_sum=None,
            disk_aloc_sum=None,

    ):
        self.name = name
        self.endpoint=endpoint
        self.cluster_metadata=cluster_metadata
        self.maintenance_mode = maintenance_mode
        self.cloud_metadata = cloud_metadata
        self.cpu_sum = cpu_sum  # unit => 1 core = 1000 number for example 8 core 8000
        self.memory_sum = memory_sum  # unit MB
        self.disk_sum = disk_sum  # unit GB
        self.cpu_aloc_sum = cpu_aloc_sum
        self.memory_aloc_sum = memory_aloc_sum
        self.disk_aloc_sum = disk_aloc_sum

        if cluster_type and isinstance(cluster_type, ClusterType):
            self.cluster_type_id = cluster_type.id
        elif cluster_type and isinstance(cluster_type, int):
            self.cluster_type_id = cluster_type

        if datacenter and isinstance(datacenter, DataCenter):
            self.datacenter_id = datacenter.id
        elif datacenter and isinstance(datacenter, int):
            self.datacenter_id = datacenter

    def select_cluster_network_ip(self ,
                                  project,
                                  container):
        network_ip = None
        selected_ip = None
        network_subnet_selected = None
        networks = Network.cluster.query.filter(and_(Network.cluster_id == self.id,Network.external_network==True)).all()

        continue_get_ip=True
        while (not selected_ip) and continue_get_ip :
            for network in networks:
                network_subnets = NetworkSubnet.query.filter(NetworkSubnet.network_id == network.id).all()
                for network_subnet in network_subnets:
                    alloc_ip_addresses = NetworkIp.query(NetworkIp.ip_address) \
                        .filter(NetworkIp.network_subnet_id == network_subnet).all()
                    alloc_ip_addresses = [IPAddress(ip_address) for ip_address in alloc_ip_addresses]

                    for allocation_pool in network_subnet._allocation_pools:
                        allocation_pool_list = list(IPRange(allocation_pool.first_ip, allocation_pool.last_ip))
                        un_alloc_ips = list(set(allocation_pool_list).difference(alloc_ip_addresses))
                        if un_alloc_ips:
                            selected_ip = un_alloc_ips[0]
                            network_subnet_selected = network_subnet
                            break
                    if selected_ip:
                        break
                if selected_ip:
                    break

            if selected_ip:
                # Add network_ip to database
                try:
                    network_ip = NetworkIp(ip_address=str(selected_ip),
                                           project=project,
                                           network_subnet=network_subnet_selected,
                                           container=container)
                    db.session.add(network_ip)
                    db.session.commit()
                except Exception:
                    selected_ip=None
                else:
                    selected_ip = None
            else:
                continue_get_ip=False

        return network_ip

    def save_lxd_cluster_cert(self,cert_crt,cert_key):

        cert_config=get_cert_path()
        cert_directory=cert_config['cert_path']
        if not os.path.exists(cert_directory):
            os.makedirs(cert_directory)

        crt_file_path=os.path.join(cert_directory,"{0}_{1}.crt".format(self.name,self.id))
        key_file_path = os.path.join(cert_directory, "{0}_{1}.key".format(self.name, self.id))
        if cert_crt:
            with open(crt_file_path, 'w') as f:
                f.write(cert_crt)
        if cert_key:
            with open(key_file_path, 'w') as f :
                f.write(cert_key)

        return crt_file_path, key_file_path

    def get_lxd_cluster_cert(self):

        cert_config=get_cert_path()
        cert_directory=cert_config['cert_path']

        crt_file_path=os.path.join(cert_directory,"{0}_{1}.crt".format(self.name,self.id))
        key_file_path = os.path.join(cert_directory, "{0}_{1}.key".format(self.name, self.id))
        cert = (crt_file_path, key_file_path)
        return cert


    def __jsonapi__(self, info=None):
        datacenter = _datacenter_find(self.datacenter_id)
        cluster_type= _cluster_types_find(self.cluster_type_id)
        _json = {
            'type': 'clusters',
            'id': self.id,
            'name': self.name,
            'endpoint': self.endpoint,
            'maintenance_mode': self.maintenance_mode,
            'cluster_metadata' : self.cluster_metadata,
            'cloud_metadata': self.cloud_metadata,
            'cpu_sum': self.cpu_sum,  # unit => 1 core = 1000 number for example 8 core 8000
            'memory_sum': self.memory_sum,  # unit MB
            'disk_sum': self.disk_sum,  # unit GB
            'cpu_aloc_sum': self.cpu_aloc_sum,
            'memory_aloc_sum': self.memory_aloc_sum,
            'disk_aloc_sum': self.disk_aloc_sum,
            'cluster_type': {'id': cluster_type.id, 'name': cluster_type.name},
            'datacenter': {'id': datacenter.id, 'name': datacenter.name},
        }
        return _json

    def __repr__(self):
        return '<Cluster %r>' % self.id


class ClusterType(db.Model):
    __tablename__ = 'cluster_types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    # uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cluster_metadata_template=db.Column(db.JSON)
    cloud_metadata = db.Column(db.JSON)

    def __init__(
            self,
            name,
            cluster_metadata_template=None,
            cloud_metadata=None,

    ):
        self.name = name
        self.cluster_metadata_template = cluster_metadata_template
        self.cloud_metadata = cloud_metadata

    def __jsonapi__(self, info=None):
        _json = {
            'type': 'cluster_types',
            'id': self.id,
            'name': self.name,
            'cluster_metadata_template': self.cluster_metadata_template,
            'cloud_metadata': self.cloud_metadata

        }
        return _json

    def __repr__(self):
        return '<ClusterType %r>' % self.id


class Host(db.Model):
    __tablename__ = 'hosts'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cloud_metadata = db.Column(db.JSON)
    maintenance_mode = db.Column(db.Boolean)
    cpu = db.Column(db.Integer) # unit => 1 core = 1000 number for example 8 core 8000
    memory = db.Column(db.Integer) # unit MB 
    disk = db.Column(db.Integer) # unit GB
    cpu_share=db.Column(db.Integer)
    memory_share = db.Column(db.Integer)
    cpu_aloc= db.Column(db.Integer)
    memory_aloc = db.Column(db.Integer)
    disk_aloc = db.Column(db.Integer)

    host_type_id = db.Column(db.Integer, db.ForeignKey('host_types.id'), nullable=False)
    db.relationship('HostTypes', uselist=False, backref='containers', lazy='dynamic')

    cluster_id = db.Column(db.Integer, db.ForeignKey('clusters.id'), nullable=False)
    db.relationship('Cluster', uselist=False, backref='containers', lazy='dynamic')

    def __init__(
            self,
            name,
            host_type,
            cluster,
            maintenance_mode=True,
            cloud_metadata=None,
            cpu=None, # unit => 1 core = 1000 number for example 8 core 8000
            memory = None,  # unit MB
            disk = None, # unit GB
            cpu_share = None,
            memory_share = None,
            cpu_aloc = None,
            memory_aloc = None,
            disk_aloc = None,

    ):
        self.name = name
        self.maintenance_mode=maintenance_mode
        self.cloud_metadata = cloud_metadata
        self.cpu = cpu  # unit => 1 core = 1000 number for example 8 core 8000
        self.memory = memory # unit MB
        self.disk = disk  # unit GB
        self.cpu_share = cpu_share
        self.memory_share = memory_share
        self.cpu_aloc = cpu_aloc
        self.memory_aloc = memory_aloc
        self.disk_aloc = disk_aloc


        if host_type and isinstance(host_type, HostType):
            self.host_type_id = host_type.id
        elif host_type and isinstance(host_type, int):
            self.host_type_id = host_type

        if cluster and isinstance(cluster, Cluster):
            self.cluster_id = cluster.id
        elif cluster and isinstance(cluster, int):
            self.cluster_id = cluster


    def __jsonapi__(self, info=None):
        cluster= _cluster_find(self.cluster_id)
        host_type= _host_types_find(self.host_type_id)
        _json = {
            'type': 'hosts',
            'id': self.id,
            'name': self.name,
            'maintenance_mode': self.maintenance_mode,
            'cloud_metadata': self.cloud_metadata,
            'cpu' : self.cpu, # unit => 1 core = 1000 number for example 8 core 8000
            'memory' : self.memory, # unit MB
            'disk' : self.disk ,  # unit GB
            'cpu_share' : self.cpu_share,
            'memory_share' : self.memory_share,
            'cpu_aloc': self.cpu_aloc,
            'memory_aloc' :self.memory_aloc,
            'disk_aloc': self.disk_aloc,
            'cluster': {'id' :cluster.id , 'name': cluster.name },
            'host_type': {'id': host_type.id, 'name': host_type.name}
        }
        return _json

    def __repr__(self):
        return '<Host %r>' % self.id


class HostType(db.Model):
    __tablename__ = 'host_types'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    # uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cloud_metadata = db.Column(db.JSON)

    def __init__(
            self,
            name=None,
            cloud_metadata=None,

    ):
        self.name = name
        self.cloud_metadata = cloud_metadata

    def __jsonapi__(self, info=None):
        _json = {
            'type': 'host_types',
            'id': self.id,
            'name': self.name,
            'cloud_metadata': self.cloud_metadata

        }
        return _json

    def __repr__(self):
        return '<HostType %r>' % self.id



class Flavor(db.Model):
    __tablename__ = 'flavors'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    # uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cpu = db.Column(db.Integer)  # unit => 1 core = 1000 number for example 8 core 8000
    memory = db.Column(db.Integer)  # unit MB
    disk = db.Column(db.Integer)  # unit GB
    traffic = db.Column(db.Integer)
    cloud_metadata = db.Column(db.JSON)

    def __init__(
            self,
            name,
            cloud_metadata=None,
            cpu=0,  # unit => 1 core = 1000 number for example 8 core 8000
            memory= 0,  # unit MB
            disk = 0, # unit GB
            traffic = 0,
    ):
        self.name = name
        self.cloud_metadata = cloud_metadata
        self.cpu = cpu
        self.memory = memory
        self.disk = disk
        self.traffic = traffic

    def __jsonapi__(self, info=None):
        _json = {
            'type': 'flavors',
            'id': self.id,
            'name': self.name,
            'cloud_metadata': self.cloud_metadata,
            'cpu' : self.cpu,  # unit => 1 core = 1000 number for example 8 core 8000
            'memory' :self.memory,  # unit MB
            'disk' : self.disk , # unit GB
            'traffic' : self.traffic
        }
        return _json

    def __repr__(self):
        return '<Flavor %r>' % self.id


class ClusterFlavorHostType(db.Model):
    __tablename__ = 'cluster_flavor_host_types'
    id = db.Column(db.Integer, primary_key=True)
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)

    cluster_id = db.Column(db.Integer, db.ForeignKey('clusters.id'), nullable=False)
    flavor_id = db.Column(db.Integer, db.ForeignKey('flavors.id'), nullable=False)
    host_type_id = db.Column(db.Integer, db.ForeignKey('host_types.id'), nullable=False)

    price=db.Column(db.Integer, primary_key=True)
    cloud_metadata = db.Column(db.JSON)

    db.UniqueConstraint('cluster_id', 'flavor_id','host_type_id')
    db.relationship('Cluster', uselist=False, backref='cluster_flavor_host_types', lazy='dynamic')
    db.relationship('Flavor', uselist=False, backref='cluster_flavor_host_types', lazy='dynamic')
    db.relationship('HostTypes', uselist=False, backref='cluster_flavor_host_types', lazy='dynamic')

    def __init__(self,
                 cluster,
                 flavor,
                 host_type,
                 price):
        if cluster and isinstance(cluster, Cluster):
            self.cluster_id = cluster.id
        elif cluster and isinstance(cluster, int):
            self.cluster_id = cluster

        if flavor and isinstance(flavor, Flavor):
            self.flavor_id = flavor.id
        elif flavor and isinstance(flavor, int):
            self.flavor_id = flavor

        if host_type and isinstance(host_type, HostType):
            self.host_type_id = host_type.id
        elif host_type and isinstance(host_type, int):
            self.host_type_id = host_type

        self.price=price

    def __jsonapi__(self, info=None):
        cluster = _cluster_find(self.cluster_id)
        host_type = _host_types_find(self.host_type_id)
        flavor=_flavor_find(self.flavor_id)

        _json = {
            'type': 'cluster_flavor_host_types',
            'id': self.id,
            'cloud_metadata': self.cloud_metadata,
            'price': self.price,
            'cluster': {'id': cluster.id,'name': cluster.name},
            'flavor': {'id': flavor.id,'name': flavor.name},
            'host_type':{'id': host_type.id,'name': host_type.name},
        }
        return _json



    def __repr__(self):
        return "<ClusterFlavorHostType(%s)>"



class Network(db.Model):
    __tablename__ = 'networks'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    maintenance_mode = db.Column(db.Boolean)
    external_network = db.Column(db.Boolean)
    cloud_metadata = db.Column(db.JSON)
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    ipcost=db.Column(db.Integer)
    portcost=db.Column(db.Integer)
    tarfficcost=db.Column(db.Integer)

    cluster_id = db.Column(db.Integer, db.ForeignKey('clusters.id'), nullable=False)
    db.relationship('Cluster', uselist=False, backref='networks', lazy='dynamic')

    def __init__(
            self,
            name=None,
            maintenance_mode=True,
            external_network=True,
            cloud_metadata=None,
            ipcost=None,
            portcost=None,
            tarfficcost=None,
            cluster=None,
    ):
        self.name = name
        self.maintenance_mode=maintenance_mode
        self.external_network = external_network
        self.cloud_metadata = cloud_metadata
        self.ipcost = ipcost
        self.portcost = portcost
        self.tarfficcost = tarfficcost



        if cluster and isinstance(cluster, Cluster):
            self.cluster_id = cluster.id
        elif cluster and isinstance(cluster, int):
            self.cluster_id = cluster

    def __jsonapi__(self, info=None):
        cluster = _cluster_find(self.cluster_id)
        _json = {
            'type': 'networks',
            'id': self.id,
            'name': self.name,
            'maintenance_mode': self.maintenance_mode,
            'external_network': self.external_network,
            'cloud_metadata': self.cloud_metadata,
            'ipcost': self.ipcost,
            'portcost': self.portcost,
            'tarfficcost': self.tarfficcost,
            'cluster': {'id': cluster.id, 'name': cluster.name},
        }
        return _json

    def __repr__(self):
        return '<Network %r>' % self.id





class NetworkSubnet(db.Model):
    __tablename__ = 'network_subnets'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    maintenance_mode = db.Column(db.Boolean)
    cloud_metadata = db.Column(db.JSON)
    cidr = db.Column(db.String(255))
    # allocation_pools= db.Column(db.String(255))
    dns_nameservers=db.Column(db.String(255))
    enable_dhcp=db.Column(db.Boolean)
    gateway_ip=db.Column(db.String(255))
    host_routes=db.Column(db.String(255))
    network_id = db.Column(db.Integer, db.ForeignKey('networks.id'), nullable=False)
    db.relationship('Network', uselist=False, backref='network_subnets', lazy='dynamic')

    _allocation_pools = db.relationship('IPAllocationPool',backref='subnet',lazy="subquery",cascade='delete')

    allocation_pools = association_proxy(
        '_allocation_pools',
        'id',
        creator=_ip_allocation_pool_find
    )


    def __init__(
            self,
            name,
            cidr,
            maintenance_mode=True,
            cloud_metadata=None,
            allocation_pools=None,
            dns_nameservers=None,
            enable_dhcp=None,
            gateway_ip=None,
            host_routes=None,
            network=None,
    ):
        self.name = name
        self.cloud_metadata = cloud_metadata
        self.cidr = cidr
        self.maintenance_mode=maintenance_mode
        self.allocation_pools = allocation_pools
        self.dns_nameservers = dns_nameservers
        self.enable_dhcp = enable_dhcp
        self.gateway_ip = gateway_ip
        self.host_routes = host_routes

        if allocation_pools and isinstance(allocation_pools, list):
            self.allocation_pools = [allocation_pool for allocation_pool in allocation_pools]
        elif allocation_pools and isinstance(allocation_pools, int):
            self.allocation_pools = [allocation_pools]


        if network and isinstance(network, Network):
            self.network_id = network.id
        elif network and isinstance(network, int):
            self.network_id = network

    def __jsonapi__(self, info=None):
        network = _network_find(self.network_id)
        _json = {'type': 'network_subnets',
                 'id': self.id,
                 'name': self.name,
                 'maintenance_mode': self.maintenance_mode,
                 'cloud_metadata': self.cloud_metadata,
                 'cidr': self.cidr, 'dns_nameservers': self.dns_nameservers,
                 'enable_dhcp': self.enable_dhcp,
                 'gateway_ip': self.gateway_ip,
                 'network': {'id': network.id, 'name': network.name},
                 'allocation_pools': [alloc.__jsonapi__() for alloc in self._allocation_pools]
                 }
        return _json

    def __repr__(self):
        return '<NetworkSubnet %r>' % self.id

class IPAllocationPool(db.Model):
    """Representation of an allocation pool in a subnet."""

    __tablename__ = 'ip_allocation_pools'
    id = db.Column(db.Integer, primary_key=True)

    subnet_id = db.Column(db.String(36), db.ForeignKey('network_subnets.id',ondelete="CASCADE"),nullable=True)
    first_ip = db.Column(db.String(64), nullable=False)
    last_ip = db.Column(db.String(64), nullable=False)

    def __jsonapi__(self, info=None):
        _json = {'type': 'ip_allocation_pools',
                 'id': self.id,
                 'subnet_id': self.subnet_id,
                 'first_ip': self.first_ip,
                 'last_ip': self.last_ip,
                 }
        return _json

    def __repr__(self):
        return "%s - %s" % (self.first_ip, self.last_ip)


class NetworkIp(db.Model):
    __tablename__ = 'network_ips'
    id = db.Column(db.Integer, primary_key=True)
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cloud_metadata = db.Column(db.JSON)
    ip_address=db.Column(db.String(255), unique=True)
    port_sharing= db.Column(db.Boolean, default=False, nullable=False)


    network_subnet_id = db.Column(db.Integer, db.ForeignKey('network_subnets.id'), nullable=False)

    db.relationship('NetworkSubnet', uselist=False, backref='network_ips', lazy='dynamic')

    container_id = db.Column(db.Integer, db.ForeignKey('containers.id'), nullable=True)
    db.relationship('Container', uselist=False, backref='network_ips', lazy='dynamic')

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=True)
    db.relationship('Project', uselist=False, backref='containers', lazy='dynamic')

    host_id = db.Column(db.Integer, db.ForeignKey('hosts.id'), nullable=True)
    db.relationship('Host', uselist=False, backref='containers', lazy='dynamic')

    def __init__(
            self,
            ip_address,
            project,
            network_subnet,
            port_sharing=False,
            container=None,
            host=None,
            cloud_metadata=None,
    ):
        self.cloud_metadata = cloud_metadata
        self.ip_address = ip_address
        self.port_sharing = port_sharing

        if project and isinstance(project, Project):
            self.project_id = project.id
        elif project and isinstance(project, int):
            self.project_id = project

        if network_subnet and isinstance(network_subnet, NetworkSubnet):
            self.network_subnet_id = network_subnet.id
        elif network_subnet and isinstance(network_subnet, int):
            self.network_subnet_id = network_subnet

        if container and isinstance(container, Container):
            self.container_id = container.id
        elif container and isinstance(container, int):
            self.container_id = container

        if host and isinstance(host, Host):
            self.host_id = host.id
        elif host and isinstance(host, int):
            self.host_id = host




    def __jsonapi__(self, info=None):
        network_subnet = _network_subnet_find(self.network_id)
        container = _container_find(self.container_id)
        host = _host_find(self.host_id)
        project = _project_find(self.project_id)
        _json = {
            'type': 'network_ips',
            'id': self.id,
            'cloud_metadata': self.cloud_metadata,
            'ip_address':self.ip_address,
            'port_sharing': self.port_sharing,
            'project': {'id': project.id, 'name': project.name},
            'network_subnet': {'id': network_subnet.id, 'name': network_subnet.name},
            'container': {'id': container.id, 'name': container.name},
            'host': {'id': host.id, 'name': host.name},
        }
        return _json

    def __repr__(self):
        return '<NetworkIp %r>' % self.id


class NetworkProxy(db.Model):
    __tablename__ = 'network_proxys'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    #uuid = db.Column(UUID(as_uuid=True), unique=True, nullable=False)
    cloud_metadata = db.Column(db.JSON)
    container_port_num=db.Column(db.Integer)
    share_port_num = db.Column(db.Integer)


    db.UniqueConstraint('share_network_ip_id','share_port_num')
    db.UniqueConstraint('container_network_ip_id', 'container_port_num')

    share_network_ip_id = db.Column(db.Integer, db.ForeignKey('network_ips.id'), nullable=False)
    db.relationship('NetworkIp', uselist=False, backref='network_proxys', lazy='dynamic')

    container_network_ip_id = db.Column(db.Integer, db.ForeignKey('network_ips.id'), nullable=False)
    db.relationship('NetworkIp', uselist=False, backref='network_proxys', lazy='dynamic')

    container_id = db.Column(db.Integer, db.ForeignKey('containers.id'), nullable=True)
    db.relationship('Container', uselist=False, backref='network_proxys', lazy='dynamic')

    project_id = db.Column(db.Integer, db.ForeignKey('projects.id'), nullable=True)
    db.relationship('Project', uselist=False, backref='network_proxys', lazy='dynamic')

    
    def __init__(
            self,
            name ,
            container_port_num,
            share_port_num,
            container_network_ip,
            share_network_ip,
            container,
            project,
            cloud_metadata=None,
    ):
        self.name = name
        self.cloud_metadata = cloud_metadata
        self.container_port_num = container_port_num
        self.share_port_num = share_port_num



        if container_network_ip and isinstance(container_network_ip, NetworkIp):
            self.container_network_ip_id = container_network_ip.id
        elif container_network_ip and isinstance(container_network_ip, int):
            self.container_network_ip_id = container_network_ip

        if share_network_ip and isinstance(share_network_ip, NetworkIp):
            self.share_network_ip_id = share_network_ip.id
        elif share_network_ip and isinstance(share_network_ip, int):
            self.share_network_ip_id = share_network_ip

        if project and isinstance(project, Project):
            self.project_id = project.id
        elif project and isinstance(project, int):
            self.project_id = project

        if container and isinstance(container, Container):
            self.container_id = container.id
        elif container and isinstance(container, int):
            self.container_id = container

    def __jsonapi__(self, info=None):
        container_network_ip = _network_ip_find(self.container_network_ip_id)
        share_network_ip = _network_ip_find(self.share_network_ip_id)
        container=_container_find(self.container_id)
        project = _project_find(self.container_id)

        _json = {
            'type': 'network_proxys',
            'id': self.id,
            'name': self.name,
            'cloud_metadata': self.cloud_metadata,
            'container_port_num': self.container_port_num,
            'share_port_num': self.share_port_num,
            'container': {'id': container.id, 'name': container.name},
            'container_network_ip': {'id': container_network_ip.id, 'name': container_network_ip.name},
            'share_network_ip': {'id': share_network_ip.id, 'name': share_network_ip.name},
            'project': {'id': project.id, 'name': project.name},
        }
        return _json

    def __repr__(self):
        return '<NetworkProxy %r>' % self.id