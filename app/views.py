#!/usr/bin/env python
# -*- coding: utf-8 -*-
from flask import request
from flask_restplus import Resource
from flask_jwt_extended import fresh_jwt_required, jwt_required, create_access_token, \
    jwt_refresh_token_required, create_refresh_token, get_jti, get_raw_jwt, get_jwt_identity
from app import db, api, redis_store, app
from sqlalchemy import and_

from .decorators import *
from .fields.auth import *
from .fields.users import *
from .fields.groups import *
from .fields.projects import *
from .fields.roles import *
from .fields.role_abilities import *
from .fields.users_roles_projects import *
from .fields.abilities import *
from .fields.requests import *
from .fields.lxdconfig import *
from .fields.lxdcerts import *
import lgw
import time
import configparser


class Auth(Resource):

    @api.marshal_with(auth_fields_get)
    @api.expect(auth_fields_post, validate=True)
    def post(self):
        """
        Get Json Web Token without confirmation
        :return access_token
        :return refresh_token
        """
        request_data = request.get_json()
        username = request_data['username']
        password = request_data['password']

        user = User.query.filter_by(username=username).first()

        if not user or not user.verify_password(password):
            api.abort(code=401, message='Incorrect user or password')

        user.otp_confirmed = True

        # use if user has otp enabled
        if user.get_otp_type():
            if user.get_otp_type() == 'email':
                eotp_secret = user.create_eotp()
                # print(eotp_secret)
                lgw.send_otp_email(eotp_secret, user.email)  # send email with otp
            user.otp_confirmed = False
            access_token = create_access_token(identity=user, fresh=False)
            ret = {'access_token': access_token}
            access_jti = get_jti(encoded_token=access_token)
            redis_store.set('access_jti:' + access_jti, 'false', app.config['OTP_ACCESS_TOKEN_EXPIRES'])
            # print(redis_store.get(access_jti) + ' ' + access_jti)
            return ret

        access_token = create_access_token(identity=user, fresh=True)
        refresh_token = create_refresh_token(identity=user)
        access_jti = get_jti(encoded_token=access_token)
        refresh_jti = get_jti(encoded_token=refresh_token)
        redis_store.set('access_jti:' + access_jti, 'false', app.config['ACCESS_TOKEN_EXPIRES'])
        redis_store.set('refresh_jti:' + refresh_jti, 'false', app.config['REFRESH_TOKEN_EXPIRES'])
        ret = {'access_token': access_token,
               'refresh_token': refresh_token}
        return ret


class AuthOtp(Resource):
    decorators = [jwt_required]

    @api.marshal_with(auth_otp_fields_get)
    @api.expect(auth_otp_fields_post, validate=True)
    def post(self):
        """
        Get Json Web Token with confirmation (after correct OTP key)
        :return new access_token
        :return new refresh_token
        """
        request_data = request.get_json()
        secret = request_data['secret']
        # print(secret)
        user = import_user()

        # revoke otp_access_token
        jti = get_raw_jwt()['jti']
        redis_store.set('access_jti:' + jti, 'true', app.config['OTP_ACCESS_TOKEN_EXPIRES'])

        if user.get_otp_type() == 'totp':
            if not user.verify_totp(secret):
                api.abort(code=401, message='Incorrect secret code')
        elif user.get_otp_type() == 'email':
            if not user.verify_eotp(secret):
                api.abort(code=401, message='Incorrect secret code')
        else:
            api.abort(code=401, message='Incorrect otp type')

        user.otp_confirmed = True

        access_token = create_access_token(identity=user, fresh=True)
        refresh_token = create_refresh_token(identity=user)
        access_jti = get_jti(encoded_token=access_token)
        refresh_jti = get_jti(encoded_token=refresh_token)
        redis_store.set('access_jti:' + access_jti, 'false', app.config['ACCESS_TOKEN_EXPIRES'])
        redis_store.set('refresh_jti:' + refresh_jti, 'false', app.config['REFRESH_TOKEN_EXPIRES'])
        ret = {'access_token': access_token,
               'refresh_token': refresh_token}
        return ret


class AuthRefresh(Resource):
    decorators = [jwt_refresh_token_required]

    #@api.marshal_with(auth_fields_get)
    def post(self):
        """
        Get new token with valid token
        :return new access_token
        """
        user = import_user()
        user.otp_confirmed = True
        access_token = create_access_token(identity=user, fresh=False)
        access_jti = get_jti(encoded_token=access_token)
        redis_store.set('access_jti:' + access_jti, 'false', app.config['ACCESS_TOKEN_EXPIRES'])
        ret = {
            'access_token': access_token
        }
        return ret


class AuthCheck(Resource):
    decorators = [jwt_required, otp_confirmed]

    @api.doc(responses={
        200: 'Token OK',
        401: 'Token invalid or expired',
        422: 'Signature verification failed'
    })
    def get(self):
        """
        Check token
        """
        return {}, 200


# Endpoint for revoking the current users access token
class AuthLogout(Resource):
    decorators = [jwt_required]

    def delete(self):
        """
        Revoke token
        """
        jti = get_raw_jwt()['jti']
        if not jti:
            api.abort(code=404, message='Token not found')
        redis_store.set('access_jti:' + jti, 'true', app.config['ACCESS_TOKEN_EXPIRES'])
        return {"msg": "Access token revoked"}, 200


class UsersList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('users_infos_all')
    @api.marshal_with(users_fields_get_many)
    def get(self):
        """
        Get users list
        """
        populate_containers_table()
        users = User.query.all()
        users_list = []

        for user in users:
            users_list.append(user.__jsonapi__())

        return {'data': users_list}

    @user_has('users_create')
    @api.expect(users_fields_post, validate=True)
    @api.marshal_with(users_fields_get)
    def post(self):
        """
        Create user
        """
        current_identity = import_user()
        data = request.get_json()['data']
        if User.query.filter_by(username=data['username']).first():
            api.abort(code=409, message='User already exists')

        user = User()

        user.username = data['username']
        user.name = data['name']
        user.hash_password(data['password'])

        if 'admin' in data and current_identity.admin:
            user.admin = data['admin']
        if 'email' in data:
            user.email = data['email']
        if 'phone' in data:
            user.phone = data['phone']
        if 'address' in data:
            user.address = data['address']
        if 'city' in data:
            user.city = data['city']
        if 'country' in data:
            user.country = data['country']
        if 'postal_code' in data:
            user.postal_code = data['postal_code']
        if 'ico' in data:
            user.ico = data['ico']
        if 'ic_dph' in data:
            user.ic_dph = data['ic_dph']
        if 'dic' in data:
            user.dic = data['dic']
        if 'language' in data:
            user.language = data['language']
        if 'otp_type' in data:
            if data['otp_type'] == 'none':
                user.otp_type = None
            else:
                user.otp_type = data['otp_type']


        db.session.add(user)
        db.session.commit()

        return {'data': user.__jsonapi__()}, 201


class Users(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('users_infos')
    @api.marshal_with(users_fields_get)
    def get(self, id):
        """
        Get user
        """
        populate_containers_table()
        user = User.query.get(id)

        if not user:
            api.abort(code=404, message='User not found')

        return {'data': user.__jsonapi__()}

    @user_has('users_update')
    @api.expect(users_fields_put, validate=True)
    @api.marshal_with(users_fields_get)
    def put(self, id):
        """
        Update user
        """
        current_identity = import_user()
        user = User.query.get(id)

        if not user:
            api.abort(code=404, message='User not found')

        data = request.get_json()['data']

        if 'admin' in data and current_identity.admin:
            user.admin = data['admin']
        if 'name' in data:
            user.name = data['name']
        if 'email' in data:
            user.email = data['email']
        if 'phone' in data:
            user.phone = data['phone']
        if 'address' in data:
            user.address = data['address']
        if 'city' in data:
            user.city = data['city']
        if 'country' in data:
            user.country = data['country']
        if 'postal_code' in data:
            user.postal_code = data['postal_code']
        if 'ico' in data:
            user.ico = data['ico']
        if 'ic_dph' in data:
            user.ic_dph = data['ic_dph']
        if 'dic' in data:
            user.dic = data['dic']
        if 'language' in data:
            user.language = data['language']

        if 'password' in data and current_identity.admin:
            user.hash_password(data['password'])

        try:
            user.groups = list(id['id'] for id in data[
                               'relationships']['groups'])
        except KeyError:
            pass

        try:
            user.containers = list(id['id'] for id in data[
                                   'relationships']['containers'])
        except KeyError:
            pass

        if len(data) > 0:
            db.session.commit()

        return {'data': user.__jsonapi__()}

    @user_has('users_delete')
    def delete(self, id):
        """
        Delete user
        """
        user = User.query.get(id)

        if not user:
            api.abort(code=404, message='User not found')

        db.session.delete(user)
        db.session.commit()

        return {}, 204


class Me(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('me_infos')
    @api.marshal_with(users_fields_get)
    def get(self):
        """
        Get me
        """
        populate_containers_table()
        current_identity = import_user()
        return {'data': current_identity.__jsonapi__()}

    @user_has('me_update')
    #@api.expect(users_fields_put, validate=True)
    @api.marshal_with(users_fields_get)
    def put(self):
        """
        Update me
        """
        current_identity = import_user()
        user = User.query.get(current_identity.id)

        data = request.get_json()['data']

        if 'name' in data:
            user.name = data['name']
        if 'email' in data:
            user.email = data['email']
        if 'phone' in data:
            user.phone = data['phone']
        if 'address' in data:
            user.address = data['address']
        if 'city' in data:
            user.city = data['city']
        if 'country' in data:
            user.country = data['country']
        if 'postal_code' in data:
            user.postal_code = data['postal_code']
        if 'ico' in data:
            user.ico = data['ico']
        if 'ic_dph' in data:
            user.ic_dph = data['ic_dph']
        if 'dic' in data:
            user.dic = data['dic']
        if 'langugage' in data:
            user.language = data['language']
        if 'otp_type' in data:
            if data['otp_type'] == 'none':
                user.otp_type = None
            else:
                user.otp_type = data['otp_type']

        if 'cur_password' in data:
            cur_password = data['cur_password']
            #print('before verify')
            #print(data['attributes'])
            if 'new_password' in data and user.verify_password(cur_password):
                if data['new_password'] == data['confirm_password']:
                    user.hash_password(data['new_password'])
            else:
                api.abort(code=401, message='Incorrect user or password')

        # Not secure for user to change his group or container

        if len(data) > 0:
            db.session.commit()

        return {'data': user.__jsonapi__()}

    # Stupid - user delete himself?
    #@user_has('me_edit')
    #def delete(self):
    #    """
    #    Delete me (stupid)
    #    """
    #    current_identity = import_user()
    #    user = User.query.get(current_identity.id)
    #
    #    db.session.delete(user)
    #    db.session.commit()
    #
    #    return {}, 204


class MeOtp(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('me_otp_create')
    def post(self):
        """
        Generate totp secret
        User can set totp secret only once
        """
        current_identity = import_user()
        user = User.query.get(current_identity.id)

        if not user:
            api.abort(code=404, message='User not found')

        # if user has otp_secret do not create new
        if not user.otp_type:
            try:
                user.otp_type = 'totp'
                user.add_totp_secret()
            except Exception as e:
                print(e)
                api.abort(code=500, message='Can\'t generate otp secret')
        else:
            api.abort(code=500, message='User has otp secret set')

        db.session.commit()
        json_ret = {'type': 'otp', 'otp_secret': user.otp_secret, 'otp_uri': user.get_totp_uri()}
        return {'data': json_ret}, 201


class GroupsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('groups_infos_all')
    @api.marshal_with(groups_fields_get_many)
    def get(self):
        """
        Get groups list
        """
        current_identity = import_user()

        groups = Group.query.all()
        groups_list = []

        for group in groups:
            if group.id in current_identity.groups or current_identity.admin:
                groups_list.append(group.__jsonapi__())

        return {'data': groups_list}

    @user_has('groups_create')
    @api.expect(groups_fields_post, validate=True)
    @api.marshal_with(groups_fields_get)
    def post(self):
        """
        Create group
        """
        data = request.get_json()['data']

        group = Group(name=data['name'])

        try:
            group.abilities = list(id['id'] for id in data[
                                   'relationships']['abilities'])
        except KeyError:
            pass

        try:
            group.users = list(id['id'] for id in data[
                'relationships']['users'])
        except KeyError:
            pass

        db.session.add(group)
        db.session.commit()

        return {'data': group.__jsonapi__()}, 201


class Groups(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('groups_infos')
    @api.marshal_with(groups_fields_get)
    def get(self, id):
        """
        Get group
        """
        group = Group.query.get(id)

        current_identity = import_user()

        if not group:
            api.abort(code=404, message='Group not found')

        if id in current_identity.groups or current_identity.admin:
            return {'data': group.__jsonapi__()}
        else:
            api.abort(code=403, message='You do not have access')

    @user_has('groups_update')
    @api.expect(groups_fields_put, validate=True)
    @api.marshal_with(groups_fields_get)
    def put(self, id):
        """
        Update group
        """
        group = Group.query.get(id)

        if not group:
            api.abort(code=404, message='Group not found')

        data = request.get_json()['data']

        if 'name' in data:
            group.name = data['name']

        try:
            group.abilities = list(id['id'] for id in data[
                                   'relationships']['abilities'])
        except KeyError:
            pass

        try:
            group.users = list(id['id'] for id in data[
                'relationships']['users'])
        except KeyError:
            pass

        if len(data) > 0:
            db.session.commit()

        return {'data': group.__jsonapi__()}

    @user_has('groups_delete')
    def delete(self, id):
        """
        Delete group
        """
        group = Group.query.get(id)

        if not group:
            api.abort(code=404, message='Group not found')

        db.session.delete(group)
        db.session.commit()

        return {}, 204


class ProjectsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('projects_infos_all')
    @api.marshal_with(projects_fields_get_many)
    def get(self):
        """
        Get projects list
        """
        current_identity = import_user()

        if current_identity.admin:
            projects = Project.query.all()
        #should be edit
        # else:
        #     projects = Project.query.filter(self.user_id==current_identity.id)

        projects_list = []

        for project in projects:
                projects_list.append(project.__jsonapi__())

        return {'data': projects_list}

    @user_has('projects_create')
    @api.expect(projects_fields_post, validate=True)
    @api.marshal_with(projects_fields_get)
    def post(self):
        """
        Create project
        """


        data = request.get_json()['data']

        project = Project(name=data['name'])
        if 'cloud_metadata' in data:
            project.cloud_metadata = data['cloud_metadata']
        if 'cpuQuota' in data:
            project.cpuQuota=data['cpuQuota']
        if 'memQuota' in data:
            project.memQuota=data['memQuota']
        if 'diskQuota' in data:
            project.diskQuota=data['diskQuota']
        if 'trafficQuota' in data:
            project.trafficQuota=data['trafficQuota']
        if 'ipQuota' in data:
            project.ipQuota=data['ipQuota']
        if 'portQuota' in data:
            project.portQuota=data['portQuota']


        db.session.add(project)
        db.session.commit()

        current_identity = import_user()
        owner_role=Role.query.filter(Role.name=="owner").first()

        user_role_project = UserRoleProject(project.id,current_identity.id,owner_role.id)

        db.session.add(user_role_project)

        db.session.commit()

        return {'data': project.__jsonapi__()}, 201

class Projects(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('project_infos')
    @api.marshal_with(projects_fields_get)
    def get(self, id):
        """
        Get project
        """
        project = Project.query.get(id)

        current_identity = import_user()

        if not project:
            api.abort(code=404, message='Project not found')

        #should edit
        #if id in current_identity.groups or current_identity.admin:
        return {'data': project.__jsonapi__()}
        # else:
        #     api.abort(code=403, message='You do not have access')

    @user_has('projects_update')
    @api.expect(projects_fields_put, validate=True)
    @api.marshal_with(projects_fields_get)
    def put(self, id):
        """
        Update project
        """
        project = Project.query.get(id)

        if not project:
            api.abort(code=404, message='Project not found')

        data = request.get_json()['data']

        if 'cloud_metadata' in data:
            project.cloud_metadata = data['cloud_metadata']
        if 'cpuQuota' in data:
            project.cpuQuota = data['cpuQuota']
        if 'memQuota' in data:
            project.memQuota = data['memQuota']
        if 'diskQuota' in data:
            project.diskQuota = data['diskQuota']
        if 'trafficQuota' in data:
            project.trafficQuota = data['trafficQuota']
        if 'ipQuota' in data:
            project.ipQuota = data['ipQuota']
        if 'portQuota' in data:
            project.portQuota = data['portQuota']


        if len(data) > 0:
            db.session.commit()

        return {'data': project.__jsonapi__()}

    @user_has('projects_delete')
    def delete(self, id):
        """
        Delete project
        """
        project = Project.query.get(id)

        if not project:
            api.abort(code=404, message='Project not found')

        db.session.delete(project)
        db.session.commit()

        return {}, 204


class RolesList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('roles_infos_all')
    @api.marshal_with(roles_fields_get_many)
    def get(self):
        """
        Get roles list
        """
        current_identity = import_user()

        if current_identity.admin:
            roles = Role.query.all()
            # should be edit
            # else:
            #     projects = Role.query.filter(self.user_id==current_identity.id)

        roles_list = []

        for role in roles:
            roles_list.append(role.__jsonapi__())

        return {'data': roles_list}

    @user_has('roles_create')
    @api.expect(roles_fields_post, validate=True)
    @api.marshal_with(roles_fields_get)
    def post(self):
        """
        Create role
        """
        data = request.get_json()['data']

        role = Role(name=data['name'])
        if 'cloud_metadata' in data:
            role.cloud_metadata = data['cloud_metadata']


        db.session.add(role)
        db.session.commit()

        return {'data': role.__jsonapi__()}, 201

class Roles(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('role_infos')
    @api.marshal_with(roles_fields_get)
    def get(self, id):
        """
        Get role
        """
        role = Role.query.get(id)

        current_identity = import_user()

        if not role:
            api.abort(code=404, message='Role not found')

        # should edit
        # if id in current_identity.groups or current_identity.admin:
        return {'data': role.__jsonapi__()}
        # else:
        #     api.abort(code=403, message='You do not have access')

    @user_has('roles_update')
    @api.expect(roles_fields_put, validate=True)
    @api.marshal_with(roles_fields_get)
    def put(self, id):
        """
        Update role
        """
        role = Role.query.get(id)

        if not role:
            api.abort(code=404, message='Role not found')

        data = request.get_json()['data']

        if 'cloud_metadata' in data:
            role.cloud_metadata = data['cloud_metadata']


        if len(data) > 0:
            db.session.commit()

        return {'data': role.__jsonapi__()}

    @user_has('roles_delete')
    def delete(self, id):
        """
        Delete role
        """
        role = Role.query.get(id)

        if not role:
            api.abort(code=404, message='Role not found')

        db.session.delete(role)
        db.session.commit()

        return {}, 204

class RoleAbilitiesList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('role_abilities_infos_all')
    @api.marshal_with(role_abilities_fields_get_many)
    def get(self):
        """
        Get role Abilities list
        """
        current_identity = import_user()

        if current_identity.admin:
            role_abilities = RoleAbility.query.all()
            # should be edit
            # else:
            #     projects = Role.query.filter(self.user_id==current_identity.id)

        roles_list = []

        for roleAbility in role_abilities:
            roles_list.append(roleAbility.__jsonapi__())

        return {'data': roles_list}

    @user_has('role_abilities_create')
    @api.expect(role_abilities_fields_post, validate=True)
    @api.marshal_with(roles_fields_get)
    def post(self):
        """
        Create role Ability
        """
        data = request.get_json()['data']

        roleAbility = RoleAbility(name=data['name'])
        if 'cloud_metadata' in data:
            roleAbility.cloud_metadata = data['cloud_metadata']

        db.session.add(roleAbility)
        db.session.commit()

        return {'data': roleAbility.__jsonapi__()}, 201

class RoleAbilities(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('roleAbility_infos')
    @api.marshal_with(role_abilities_fields_get)
    def get(self, id):
        """
        Get role Ability
        """
        role_abilities = RoleAbility.query.get(id)

        current_identity = import_user()

        if not role_abilities:
            api.abort(code=404, message='Role Ability not found')

        # should edit
        # if id in current_identity.groups or current_identity.admin:
        return {'data': role_abilities.__jsonapi__()}
        # else:
        #     api.abort(code=403, message='You do not have access')

    @user_has('role_abilities_update')
    @api.expect(roles_fields_put, validate=True)
    @api.marshal_with(role_abilities_fields_get)
    def put(self, id):
        """
        Update role Ability
        """
        role_abilities = RoleAbility.query.get(id)

        if not role_abilities:
            api.abort(code=404, message='Role Ability not found')

        data = request.get_json()['data']

        if 'cloud_metadata' in data:
            role_abilities.cloud_metadata = data['cloud_metadata']

        if len(data) > 0:
            db.session.commit()

        return {'data': role_abilities.__jsonapi__()}

    @user_has('roles_delete')
    def delete(self, id):
        """
        Delete role Ability
        """
        role_abilities = RoleAbility.query.get(id)

        if not role_abilities:
            api.abort(code=404, message='Role Ability not found')

        db.session.delete(role_abilities)
        db.session.commit()

        return {}, 204

class AllUserProjectsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('all_users_projects_infos_all')
    @api.marshal_with(users_roles_projects_fields_get_many)
    def get(self):
        """
        Get users access to  projects by group access level
        """
        current_identity = import_user()

        if current_identity.admin:
            user_role_project = UserRoleProject.query.all()
            if not user_role_project:
                api.abort(code=404, message='no exists data about relation Users and Projects with Group access level. ')
            return {'data': user_role_project.__jsonapi__()}
        else:
            api.abort(code=404, message='this User is not access to get data. ')



class UserProjectsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('users_projects_infos_all')
    @api.marshal_with(users_roles_projects_fields_get_many)
    def get(self,user_id):
        """
        Get users access to  projects by role access level
        """
        user_role_project = UserRoleProject.query \
            .filter(UserRoleProject.user_id == user_id).all()

        if not user_role_project:
            api.abort(code=404, message='not access this User to Projects.')


        return {'data': user_role_project.__jsonapi__(user_id=user_id)}




class UserRoleProjectsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('users_roles_projects_infos_all')
    @api.marshal_with(users_roles_projects_fields_get)
    def get(self,user_id,project_id):
        """
        Get One user access to  one project by which groups access level
        """
        project = Project.query.get(project_id)
        if not project:
            api.abort(code=404, message='Project not found')

        user = User.query.get(user_id)
        if not user:
            api.abort(code=404, message='User not found')

        user_role_project = UserRoleProject.query \
            .filter(and_(UserRoleProject.user_id == user.id,UserRoleProject.project_id == project.id)).all()

        if not user_role_project:
            api.abort(code=404, message='not access this User to Projects.')

        return {'data': user_role_project.__jsonapi__(project,user)}






class UserRoleProjects(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('users_roles_projects_infos')
    @api.marshal_with(users_roles_projects_fields_get)
    def get(self, project_id,user_id,role_id):
        """
        Get user and project relation with one role access level
        """
        user_role_project = UserRoleProject.query \
            .filter(and_(UserRoleProject.user_id == user_id,
                         UserRoleProject.project_id == project_id,
                         UserRoleProject.role_id == role_id)).all()

        if not user_role_project:
            api.abort(code=404, message='not access User to this Project with Group access level.')


        project = Project.query.get(project_id)
        if not project:
            api.abort(code=404, message='Project not found')

        user = User.query.get(user_id)
        if not user:
            api.abort(code=404, message='User not found')

        return {'data': user_role_project.__jsonapi__(project,user)}


    @user_has('users_roles_projects_update')
    @api.expect(projects_fields_put, validate=True)
    @api.marshal_with(users_roles_projects_fields_put)
    def put(self, project_id,user_id,role_id):
        """
        assign user to project with one role access level

        """
        project = Project.query.get(project_id)
        if not project:
            api.abort(code=404, message='Project not found')

        user = User.query.get(user_id)
        if not user:
            api.abort(code=404, message='User not found')

        role = Role.query.get(role_id)
        if not role:
            api.abort(code=404, message='Role not found')


        user_role_project = UserRoleProject(project.id,user.id,role.id)

        data = user_role_project.__jsonapi__(project,user)

        db.session.commit()

        return {'data': data}

    @user_has('users_roles_projects_delete')
    def delete(self,project_id,user_id,role_id):
        """
       Revoke assign user to project with one role
        """
        user_role_project = UserRoleProject.query\
            .filter(and_(UserRoleProject.user_id==user_id,
                         UserRoleProject.project_id==project_id,
                         UserRoleProject.role_id==role_id
                         )).all()

        if not user_role_project:
            api.abort(code=404, message='not access User to this Project with Role access level.')

        db.session.delete(user_role_project)
        db.session.commit()

        return {}, 204


class AbilitiesList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('abilities_infos_all')
    @api.marshal_with(abilities_fields_get_many)
    def get(self):
        """
        Get abilities list
        """
        abilities = Ability.query.all()
        abilities_list = []

        for ability in abilities:
            abilities_list.append(ability.__jsonapi__())

        return {'data': abilities_list}


class Abilities(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('abilities_infos')
    @api.marshal_with(abilities_fields_get)
    def get(self, id):
        """
        Get ability
        """
        ability = Ability.query.get(id)

        if not ability:
            api.abort(code=404, message='Ability not found')

        return {'data': ability.__jsonapi__()}

    @user_has('abilities_update')
    @api.expect(abilities_fields_put, validate=True)
    @api.marshal_with(abilities_fields_get)
    def put(self, id):
        """
        Update ability
        """
        ability = Ability.query.get(id)

        data = request.get_json()['data']

        try:
            if len(data['relationships']['groups']) >= 0:
                ability.groups = list(id['id'] for id in data[
                                      'relationships']['groups'])
                db.session.commit()
        except KeyError:
            pass

        return {'data': ability.__jsonapi__()}


##################
# New API Requests #
##################

class RequestsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('requests_infos_all')
    @api.marshal_with(requests_fields_get_many)
    def get(self):
        """
        Get requests list
        """
        current_identity = import_user()

        requests = Request.query.all()
        requests_list = []

        for req in requests:
            if req.id in current_identity.requests or current_identity.admin:
                requests_list.append(req.__jsonapi__())

        return {'data': requests_list}
        #return requests_list

    @user_has('requests_create')
    #@api.expect(requests_fields_post, validate=True)
    @api.marshal_with(requests_fields_get)
    def post(self):
        """
        Create request
        """
        data = request.get_json()['data']
        print(data)

        req = Request(message=data['message'])
        req.action = data['action']
        req.status = data['status']
        req.meta_data = data['meta_data']
        #req.users = list('1',)
        if 'relationships' in data:
            try:
                req.users = list(id['id'] for id in data[
                    'relationships']['users'])
            except KeyError:
                pass
        else:
            current_identity = import_user()
            req.users = list(str(current_identity.id),)

        db.session.add(req)
        db.session.commit()

        username = req.__jsonapi__()['relationships']['users'][0]['username']
        usermail = req.__jsonapi__()['relationships']['users'][0]['email']
        atr = req.__jsonapi__()

        mail_message = 'Username:' + username + \
                       '/r/nAction: ' + str(atr['action']) + \
                       '/r/nStatus: ' + str(atr['status']) + \
                       '/r/nInfo: ' + str(atr['meta_data'])

        lgw.send_request(data['message'], mail_message, usermail)
        return {'data': req.__jsonapi__()}, 201


class Requests(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('requests_infos')
    @api.marshal_with(requests_fields_get)
    def get(self, id):
        """
        Get request
        """
        req = Request.query.get(id)

        if not req:
            api.abort(code=404, message='Request not found')

        return {'data': req.__jsonapi__()}
        #return req.__jsonapi__()

    @user_has('requests_update')
    @api.expect(requests_fields_put, validate=True)
    @api.marshal_with(requests_fields_get)
    def put(self, id):
        """
        Update request
        """
        req = Request.query.get(id)

        if not req:
            api.abort(code=404, message='Request not found')

        data = request.get_json()['data']

        if 'message' in data:
            req.message = data['message']
        if 'status' in data:
            req.status = data['status']
        req.changed_on = datetime.datetime.now()

        try:
            req.users = list(id['id'] for id in data[
                'relationships']['users'])
        except KeyError:
            pass

        if len(data) > 0:
            db.session.commit()

        username = req.__jsonapi__()['relationships']['users'][0]['username']
        usermail = req.__jsonapi__()['relationships']['users'][0]['email']

        mail_message = 'User:' + username + '/r/nData: ' + str(
            req.__jsonapi__())
        lgw.send_request(data['message'], mail_message, usermail)

        return {'data': req.__jsonapi__()}

    @user_has('requests_delete')
    def delete(self, id):
        """
        Delete request
        """
        req = Request.query.get(id)

        if not req:
            api.abort(code=404, message='Request not found')

        db.session.delete(req)
        db.session.commit()

        return {}, 204


##################
# Other API #
##################
class LXDConfig(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('config_infos')
    #@api.marshal_with(lxdconfig_fields_get)
    def get(self):
        """
        Get LXD config
        :return data
        """

        current_identity = import_user()
        data = {}

        if current_identity.admin:
            Config = configparser.ConfigParser()
            try:
                Config.read('lxdconfig.conf')
                data = {}
                for each_section in Config.sections():
                    data[each_section] = {}
                    for (each_key, each_val) in Config.items(each_section):
                        data[each_section][each_key] = each_val

            except Exception as e:
                api.abort(code=404, message='Error when read config file.')

            #return {'data': {'attributes': data, 'type': 'lxdconfig', 'id': '1'}}
            return {'data': data}

        else:
            Config = configparser.ConfigParser()
            try:
                Config.read('lxdconfig.conf')
                data = {}
                for each_section in Config.sections():
                    data[each_section] = {}
                    if each_section == 'remote':
                        continue
                    if each_section == 'smtp':
                        continue
                    for (each_key, each_val) in Config.items(each_section):
                        data[each_section][each_key] = each_val

                if 'smtp' in Config:
                    data['smtp'] = {}
                    data['smtp']['enabled'] = Config['smtp']['enabled']

            except Exception as e:
                api.abort(code=404, message='Error when read config file.')

            #return {'data': {'attributes': data, 'type': 'lxdconfig', 'id': '1'}}
            return {'data': data}

        api.abort(code=404, message='You has not access')


    @user_has('config_update')
    @api.marshal_with(lxdconfig_fields_get)
    @api.expect(lxdconfig_fields_post)
    def post(self):
        """
        Set LXD config
        :return status code
        """
        current_identity = import_user()

        data = request.get_json()
        print(data)

        def updateconf(Config, section, option):
            if not Config.has_section(section):
                Config.add_section(section)

            if section in data:
                if option in data[section]:
                    Config.set(section, option, str(data[section][option]))

        if current_identity.admin and data:

            Config = configparser.ConfigParser()
            try:
                Config.read('lxdconfig.conf')

                updateconf(Config, 'remote', 'endpoint')
                updateconf(Config, 'remote', 'cert_crt')
                updateconf(Config, 'remote', 'cert_key')
                updateconf(Config, 'remote', 'verify')

                updateconf(Config, 'smtp', 'enabled')
                updateconf(Config, 'smtp', 'notify_user')
                updateconf(Config, 'smtp', 'sender')
                updateconf(Config, 'smtp', 'recipient')
                updateconf(Config, 'smtp', 'server')
                updateconf(Config, 'smtp', 'port')
                updateconf(Config, 'smtp', 'login')
                updateconf(Config, 'smtp', 'password')

                updateconf(Config, 'app', 'production_name')

                updateconf(Config, 'price', 'enabled')
                updateconf(Config, 'price', 'cpu')
                updateconf(Config, 'price', 'memory')
                updateconf(Config, 'price', 'disk')
                updateconf(Config, 'price', 'discount_month')
                updateconf(Config, 'price', 'discount_months')
                updateconf(Config, 'price', 'discount_halfyear')
                updateconf(Config, 'price', 'discount_year')
                updateconf(Config, 'price', 'discount_years')

                updateconf(Config, 'storage', 'enabled')
                updateconf(Config, 'storage', 'pool_name')

            except Exception as e:
                print('exception', e)
                return {}, 500

            cfgfile = open('lxdconfig.conf', 'w')
            Config.write(cfgfile)
            cfgfile.close()

        return {}, 200


class LXDCerts(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('lxd_certs_create')
    @api.marshal_with(lxdcerts_fields_get)
    @api.expect(lxdcerts_fields_post, validate=True)
    def post(self):
        """
        Update LXD connection certificates
        :return status code
        """
        current_identity = import_user()

        data = request.get_json()

        config = configparser.ConfigParser()

        try:
            config.read('lxdconfig.conf')
        except Exception as e:
            print('wrong config file')

        if current_identity.admin:
            if data['cert_crt']:
                f = open(config['remote']['cert_crt'], 'w')
                f.write(data['cert_crt'])
                f.close()

            if data['cert_key']:
                f = open(config['remote']['cert_key'], 'w')
                f.write(data['cert_key'])
                f.close()

        return {}, 200
