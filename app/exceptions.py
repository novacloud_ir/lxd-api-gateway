#!/usr/bin/env python
# -*- coding: utf-8 -*-


class UserDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ProjectDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class GroupDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class AbilityDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class RoleDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class RoleAbilityDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ContainerDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ContainerTypeDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class RequestDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class HostDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class HostTypeDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class ClusterTypeDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class FlavorDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ClusterDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class ClusterFlavorHostTypeDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class DataCenterDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class NetworkDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class IPAllocationPoolDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


class NetworkSubnetDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

class NetworkIpDoesntExist(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)

