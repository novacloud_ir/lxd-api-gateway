from flask import request
from flask_restplus import Resource
from flask_jwt_extended import fresh_jwt_required, jwt_required, create_access_token, \
    jwt_refresh_token_required, create_refresh_token, get_jti, get_raw_jwt, get_jwt_identity
from app import db, api, redis_store, app
from sqlalchemy import and_

import json

from .decorators import *
import lgw
import time
import configparser
from .fields.datacenters import *
from .fields.clusters import *
from .fields.cluster_types import *
from .fields.host_types import *
from .fields.hosts import *
from .fields.flavors import *
from .fields.networks import *
from .fields.network_subnets import *
from .fields.network_ips import *


class DataCentersList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('datacenters_infos_all')
    @api.marshal_with(datacenters_fields_get_many)
    def get(self):
        """
        Get datacenters list
        """
        current_identity = import_user()

        if current_identity.admin:
            datacenters = DataCenter.query.all()
        else:
            datacenters = DataCenter.query.filter(DataCenter.maintenance_mode==False).all()

        datacenters_list = []

        for datacenter in datacenters:
            datacenters_list.append(datacenter.__jsonapi__())

        return {'data': datacenters_list}

    @user_has('datacenters_create')
    @api.expect(datacenters_fields_post, validate=True)
    @api.marshal_with(datacenters_fields_get)
    def post(self):
        """
        Create datacenter
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']

            if 'maintenance_mode' in data:
                maintenance_mode = data['maintenance_mode']
            else:
                maintenance_mode = True

            datacenter = DataCenter(name=data['name'],maintenance_mode=maintenance_mode)
            
            if 'cloud_metadata' in data:
                datacenter.cloud_metadata = data['cloud_metadata']


            db.session.add(datacenter)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')


        return {'data': datacenter.__jsonapi__()}, 201


class DataCenters(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('datacenter_infos')
    @api.marshal_with(datacenters_fields_get)
    def get(self, id):
        """
        Get datacenter
        """
        
        datacenter = DataCenter.query.get(id)
        if not datacenter:
            api.abort(code=404, message='Data Center not found')

        current_identity = import_user()
        if not current_identity.admin:
           if  datacenter.maintenance_mode :
               api.abort(code=403, message='You do not have access.Data Center is on Maintenance Mode ')

        return {'data': datacenter.__jsonapi__()}

    @user_has('datacenters_update')
    @api.expect(datacenters_fields_put, validate=True)
    @api.marshal_with(datacenters_fields_get)
    def put(self, id):
        """
        Update datacenter
        """
        current_identity = import_user()
        if current_identity.admin:
            datacenter = DataCenter.query.get(id)
    
            if not datacenter:
                api.abort(code=404, message='Data Center not found')
    
            data = request.get_json()['data']
            if 'name' in data:
                datacenter.name = data['name']
            if 'cloud_metadata' in data:
                datacenter.cloud_metadata = data['cloud_metadata']
            if 'maintenance_mode' in data:
                datacenter.maintenance_mode = data['maintenance_mode']
        else:
            api.abort(code=403, message='You do not have access. ')


        if len(data) > 0:
            db.session.commit()

        return {'data': datacenter.__jsonapi__()}

    @user_has('datacenters_delete')
    def delete(self, id):
        """
        Delete datacenter
        """
        current_identity = import_user()
        if current_identity.admin:
            datacenter = DataCenter.query.get(id)

            if not datacenter:
                api.abort(code=404, message='Data Center not found')

            db.session.delete(datacenter)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204


class DatacenterClustersList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('clusters_infos_all')
    @api.marshal_with(clusters_fields_get_many)
    def get(self,datacenter_id):
        """
        Get clusters list by Datacenter
        """
        current_identity = import_user()

        if current_identity.admin:
            clusters = Cluster.query.filter(Cluster.datacenter_id == datacenter_id).all()
        else:
            clusters = Cluster.query.filter(
                and_(Cluster.datacenter_id == datacenter_id,Cluster.maintenance_mode==False)).all()
        # should be edit
        # else:
        #     clusters = Cluster.query.filter(self.user_id==current_identity.id)

        clusters_list = []

        if not clusters:
            api.abort(code=404, message='any Clusters not found in this Data Center')

        for cluster in clusters:
            clusters_list.append(cluster.__jsonapi__())

        return {'data': clusters_list}


class DatacenterClusterTypeClustersList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('clusters_infos_all')
    @api.marshal_with(clusters_fields_get_many)
    def get(self,datacenter_id,cluster_type_id):
        """
        Get clusters list
        """
        current_identity = import_user()

        if current_identity.admin:
            clusters = Cluster.query.filter( and_(Cluster.datacenter_id == datacenter_id,Cluster.cluster_type_id==cluster_type_id)).all()
        else:
            clusters = Cluster.query.filter(
                and_(Cluster.datacenter_id == datacenter_id,Cluster.cluster_type_id==cluster_type_id,Cluster.maintenance_mode==False)).all()
      

        clusters_list = []

        if not clusters:
            api.abort(code=404, message='any Clusters not found in this Data Center')

        for cluster in clusters:
            clusters_list.append(cluster.__jsonapi__())

        return {'data': clusters_list}

    @user_has('clusters_create')
    @api.expect(clusters_fields_post, validate=True)
    @api.marshal_with(clusters_fields_get)
    def post(self,datacenter_id,cluster_type_id):
        """
        Create cluster
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']

            cluster_type = ClusterType.query.get(cluster_type_id)
            if not cluster_type:
                api.abort(code=404, message='Cluster Type not found')

            datacenter = DataCenter.query.get(datacenter_id)
            if not datacenter:
                api.abort(code=404, message='Cluster Type not found')


            cluster = Cluster(name=data['name'],datacenter=datacenter,cluster_type=cluster_type)

            db.session.flush()

            if 'endpoint' in data:
                cluster.endpoint = data['endpoint']

            if 'lxd_metadata' in data and cluster_type.name=="lxd":
                crt_file_path, key_file_path=cluster.save_lxd_cluster_cert(data['lxd_metadata']['cert_crt'],
                                                                           data['lxd_metadata']['cert_key'])
                cluster.cluster_metadata ={'cert_crt':crt_file_path,'cert_key':key_file_path}

            if 'cloud_metadata' in data:
                cluster.cloud_metadata = data['cloud_metadata']
            if 'cpu_sum' in data:
                cluster.cpu_sum = data['cpu_sum']
            if 'memory_sum' in data:
                cluster.memory_sum = data['memory_sum']
            if 'disk_sum' in data:
                cluster.disk_sum = data['disk_sum']
            if 'cpu_aloc_sum' in data:
                cluster.cpu_aloc_sum = data['cpu_aloc_sum']
            if 'memory_aloc_sum' in data:
                cluster.memory_aloc_sum = data['memory_aloc_sum']
            if 'disk_aloc_sum' in data:
                cluster.disk_aloc_sum = data['disk_aloc_sum']

            db.session.add(cluster)
            db.session.commit()




        else:
            api.abort(code=403, message='You do not have access. ')

        return {'data': cluster.__jsonapi__()}, 201

class ClustersList(Resource):
    pass

class Clusters(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('cluster_infos')
    @api.marshal_with(clusters_fields_get)
    def get(self, id):
        """
        Get cluster
        """
        cluster = Cluster.query.get(id)



        if not cluster:
            api.abort(code=404, message='Cluster not found')

        current_identity = import_user()
        if not current_identity.admin:
            if cluster.maintenance_mode:
                api.abort(code=403, message='You do not have access. Cluster is on Maintenance Mode ')

        return {'data': cluster.__jsonapi__()}


    @user_has('clusters_update')
    @api.expect(clusters_fields_put, validate=True)
    @api.marshal_with(clusters_fields_get)
    def put(self, id):
        """
        Update cluster
        """
        current_identity = import_user()
        if current_identity.admin:
            cluster = Cluster.query.get(id)

            if not cluster:
                api.abort(code=404, message='Cluster not found')
            cluster_type = ClusterType.query.get(cluster.cluster_type_id)


            data = request.get_json()['data']
            if 'name' in data:
                cluster.name = data['name']
            if 'endpoint' in data:
                cluster.endpoint = data['endpoint']
            if 'lxd_metadata' in data and cluster_type.name == "lxd":
                cluster.cluster_metadata = json.loads(data['lxd_metadata'])
            if 'cloud_metadata' in data:
                cluster.cloud_metadata = data['cloud_metadata']
            if 'cpu_sum' in data:
                cluster.cpu_sum = data['cpu_sum']
            if 'memory_sum' in data:
                cluster.memory_sum = data['memory_sum']
            if 'disk_sum' in data:
                cluster.disk_sum = data['disk_sum']
            if 'cpu_aloc_sum' in data:
                cluster.cpu_aloc_sum = data['cpu_aloc_sum']
            if 'memory_aloc_sum' in data:
                cluster.memory_aloc_sum = data['memory_aloc_sum']
            if 'disk_aloc_sum' in data:
                cluster.disk_aloc_sum = data['disk_aloc_sum']

        else:
            api.abort(code=403, message='You do not have access. ')

        if len(data) > 0:
            db.session.commit()

        return {'data': cluster.__jsonapi__()}

    @user_has('clusters_delete')
    def delete(self, id):
        """
        Delete cluster
        """
        current_identity = import_user()
        if current_identity.admin:
            cluster = Cluster.query.get(id)

            if not cluster:
                api.abort(code=404, message='Cluster not found')

            db.session.delete(cluster)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204

class ClusterTypesList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('cluster_types_infos_all')
    @api.marshal_with(cluster_types_fields_get_many)
    def get(self):
        """
        Get cluster_types list
        """

        cluster_types = ClusterType.query.all()


        cluster_types_list = []

        for cluster_type in cluster_types:
            cluster_types_list.append(cluster_type.__jsonapi__())

        return {'data': cluster_types_list}

    @user_has('cluster_types_create')
    @api.expect(cluster_types_fields_post, validate=True)
    @api.marshal_with(cluster_types_fields_get)
    def post(self):
        """
        Create cluster_type
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']


            cluster_type = ClusterType(name=data['name'])

            if 'name' in data:
                cluster_type.name = data['name']
            if 'cluster_metadata_template' in data:
                cluster_type.cluster_metadata_template =data['cluster_metadata_template']
            if 'cloud_metadata' in data:
                cluster_type.cloud_metadata = data['cloud_metadata']

            db.session.add(cluster_type)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {'data': cluster_type.__jsonapi__()}, 201

class ClusterTypes(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('cluster_type_infos')
    @api.marshal_with(cluster_types_fields_get)
    def get(self, id):
        """
        Get cluster_type
        """

        cluster_type = ClusterType.query.get(id)
        if not cluster_type:
            api.abort(code=404, message='Cluster Type not found')


        return {'data': cluster_type.__jsonapi__()}

    @user_has('cluster_types_update')
    @api.expect(cluster_types_fields_put, validate=True)
    @api.marshal_with(cluster_types_fields_get)
    def put(self, id):
        """
        Update cluster_type
        """
        current_identity = import_user()
        if current_identity.admin:
            cluster_type = ClusterType.query.get(id)
            data = request.get_json()['data']
            if 'name' in data:
                cluster_type.name = data['name']
            if 'cluster_metadata_template' in data:
                cluster_type.cluster_metadata_template = data['cluster_metadata_template']
            if 'cloud_metadata' in data:
                cluster_type.cloud_metadata = data['cloud_metadata']
        else:
            api.abort(code=403, message='You do not have access.')

        if len(data) > 0:
            db.session.commit()

        return {'data': cluster_type.__jsonapi__()}

    @user_has('cluster_types_delete')
    def delete(self, id):
        """
        Delete cluster_type
        """
        current_identity = import_user()
        if current_identity.admin:
            cluster_type = ClusterType.query.get(id)

            if not cluster_type:
                api.abort(code=404, message='Cluster Type not found')

            db.session.delete(cluster_type)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204

class HostTypesList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('host_types_infos_all')
    @api.marshal_with(host_types_fields_get_many)
    def get(self):
        """
        Get host_types list
        """

        host_types = HostType.query.all()

        host_types_list = []

        for host_type in host_types:
            host_types_list.append(host_type.__jsonapi__())

        return {'data': host_types_list}

    @user_has('host_types_create')
    @api.expect(host_types_fields_post, validate=True)
    @api.marshal_with(host_types_fields_get)
    def post(self):
        """
        Create host_type
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']

            host_type = HostType(name=data['name'])

            if 'cloud_metadata' in data:
                host_type.cloud_metadata = data['cloud_metadata']

            db.session.add(host_type)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {'data': host_type.__jsonapi__()}, 201

class HostTypes(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('host_type_infos')
    @api.marshal_with(host_types_fields_get)
    def get(self, id):
        """
        Get host_type
        """

        host_type = HostType.query.get(id)
        if not host_type:
            api.abort(code=404, message='Host Type not found')

        return {'data': host_type.__jsonapi__()}

    @user_has('host_types_update')
    @api.expect(host_types_fields_put, validate=True)
    @api.marshal_with(host_types_fields_get)
    def put(self, id):
        """
        Update host_type
        """
        current_identity = import_user()
        if current_identity.admin:
            host_type = HostType.query.get(id)

            if not host_type:
                api.abort(code=404, message='Host Type not found')

            data = request.get_json()['data']
            if 'name' in data:
                host_type.name = data['name']
            if 'cloud_metadata' in data:
                host_type.cloud_metadata = data['cloud_metadata']
        else:
            api.abort(code=403, message='You do not have access.')

        if len(data) > 0:
            db.session.commit()

        return {'data': host_type.__jsonapi__()}

    @user_has('host_types_delete')
    def delete(self, id):
        """
        Delete host_type
        """
        current_identity = import_user()
        if current_identity.admin:
            host_type = HostType.query.get(id)

            if not host_type:
                api.abort(code=404, message='Host Type not found')

            db.session.delete(host_type)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204

class ClusterHostsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('hosts_infos_all')
    @api.marshal_with(hosts_fields_get_many)
    def get(self, cluster_id):
        """
        Get Cluster hosts list
        """
        current_identity = import_user()

        if current_identity.admin:
            hosts = Host.query.filter(Host.cluster_id == cluster_id).all()
        else:
            hosts = Host.query.filter(
                and_(Host.datacenter_id == cluster_id, Host.maintenance_mode == False)).all()


        hosts_list = []

        if not hosts:
            api.abort(code=404, message='any Hosts not found in this Cluster')

        for host in hosts:
            hosts_list.append(host.__jsonapi__())

        return {'data': hosts_list}

class ClusterHostTypeHostsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('hosts_infos_all')
    @api.marshal_with(hosts_fields_get_many)
    def get(self, cluster_id, host_type_id):
        """
        Get hosts list
        """
        current_identity = import_user()

        if current_identity.admin:
            hosts = Host.query.filter(
                and_(Host.cluster_id == cluster_id, Host.host_type_id == host_type_id)).all()
        else:
            hosts = Host.query.filter(
                and_(Host.cluster_id == cluster_id, Host.host_type_id == host_type_id,
                     Host.maintenance_mode == False)).all()

        hosts_list = []

        if not hosts:
            api.abort(code=404, message='any Hosts not found in this Cluster')

        for host in hosts:
            hosts_list.append(host.__jsonapi__())

        return {'data': hosts_list}

    @user_has('hosts_create')
    @api.expect(hosts_fields_post, validate=True)
    @api.marshal_with(hosts_fields_get)
    def post(self, cluster_id, host_type_id):
        """
        Create host
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']

            host_type = HostType.query.get(host_type_id)
            if not host_type:
                api.abort(code=404, message='Host Type not found')

            cluster = Cluster.query.get(cluster_id)
            if not cluster:
                api.abort(code=404, message='Host Type not found')

            host = Host(name=data['name'], cluster=cluster, host_type=host_type)

            if 'cloud_metadata' in data:
                host.cloud_metadata = data['cloud_metadata']
            if 'cpu' in data:
                host.cpu = data['cpu']
            if 'memory' in data:
                host.memory = data['memory']
            if 'disk' in data:
                host.disk = data['disk']
            if 'cpu_share' in data:
                host.cpu_share = data['cpu_share']
            if 'memory_share' in data:
                host.memory_share = data['memory_share']
            if 'cpu_aloc' in data:
                host.cpu_aloc = data['cpu_aloc']
            if 'memory_aloc' in data:
                host.cpu_aloc = data['memory_aloc']
            if 'disk_aloc' in data:
                host.disk_aloc = data['disk_aloc']

            db.session.add(host)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {'data': host.__jsonapi__()}, 201

class HostsList(Resource):
    pass

class Hosts(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('host_infos')
    @api.marshal_with(hosts_fields_get)
    def get(self, id):
        """
        Get host
        """
        host = Host.query.get(id)

        if not host:
            api.abort(code=404, message='Host not found')

        current_identity = import_user()
        if not current_identity.admin:
            if host.maintenance_mode:
                api.abort(code=403, message='You do not have access. Host is on Maintenance Mode ')

        return {'data': host.__jsonapi__()}

    @user_has('hosts_update')
    @api.expect(hosts_fields_put, validate=True)
    @api.marshal_with(hosts_fields_get)
    def put(self, id):
        """
        Update host
        """
        current_identity = import_user()
        if current_identity.admin:
            host = Host.query.get(id)

            if not host:
                api.abort(code=404, message='Host not found')

            data = request.get_json()['data']
            if 'name' in data:
                host.name = data['name']
            if 'cloud_metadata' in data:
                host.cloud_metadata = data['cloud_metadata']
            if 'cpu' in data:
                host.cpu = data['cpu']
            if 'memory' in data:
                host.memory = data['memory']
            if 'disk' in data:
                host.disk = data['disk']
            if 'cpu_share' in data:
                host.cpu_share = data['cpu_share']
            if 'memory_share' in data:
                host.memory_share = data['memory_share']
            if 'cpu_aloc' in data:
                host.cpu_aloc = data['cpu_aloc']
            if 'memory_aloc' in data:
                host.cpu_aloc = data['memory_aloc']
            if 'disk_aloc' in data:
                host.disk_aloc = data['disk_aloc']

        else:
            api.abort(code=403, message='You do not have access. ')

        if len(data) > 0:
            db.session.commit()

        return {'data': host.__jsonapi__()}

    @user_has('hosts_delete')
    def delete(self, id):
        """
        Delete host
        """
        current_identity = import_user()
        if current_identity.admin:
            host = Host.query.get(id)

            if not host:
                api.abort(code=404, message='Host not found')

            db.session.delete(host)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204

class FlavorsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('flavors_infos_all')
    @api.marshal_with(flavors_fields_get_many)
    def get(self):
        """
        Get flavors list
        """

        flavors = Flavor.query.all()

        flavors_list = []

        for flavor in flavors:
            flavors_list.append(flavor.__jsonapi__())

        return {'data': flavors_list}

    @user_has('flavors_create')
    @api.expect(flavors_fields_post, validate=True)
    @api.marshal_with(flavors_fields_get)
    def post(self):
        """
        Create flavor
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']

            flavor = Flavor(name=data['name'])

            if 'cloud_metadata' in data:
                flavor.cloud_metadata = data['cloud_metadata']
            if 'cpu' in data:
                flavor.cpu = data['cpu']
            if 'memory' in data:
                flavor.memory = data['memory']
            if 'disk' in data:
                flavor.disk = data['disk']
            if 'traffic' in data:
                flavor.traffic = data['traffic']



            db.session.add(flavor)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {'data': flavor.__jsonapi__()}, 201

class Flavors(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('flavor_infos')
    @api.marshal_with(flavors_fields_get)
    def get(self, id):
        """
        Get flavor
        """

        flavor = Flavor.query.get(id)
        if not flavor:
            api.abort(code=404, message='Host Type not found')

        return {'data': flavor.__jsonapi__()}

    @user_has('flavors_update')
    @api.expect(flavors_fields_put, validate=True)
    @api.marshal_with(flavors_fields_get)
    def put(self, id):
        """
        Update flavor
        """
        current_identity = import_user()
        if current_identity.admin:
            flavor = Flavor.query.get(id)

            if not flavor:
                api.abort(code=404, message='Host Type not found')

            data = request.get_json()['data']
            if 'name' in data:
                flavor.name = data['name']
            if 'cloud_metadata' in data:
                flavor.cloud_metadata = data['cloud_metadata']
            if 'cpu' in data:
                flavor.cpu = data['cpu']
            if 'memory' in data:
                flavor.memory = data['memory']
            if 'disk' in data:
                flavor.disk = data['disk']
            if 'traffic' in data:
                flavor.traffic = data['traffic']
        else:
            api.abort(code=403, message='You do not have access.')

        if len(data) > 0:
            db.session.commit()

        return {'data': flavor.__jsonapi__()}

class ClusterFlavorHostTypes(Resource):
    decorators = [jwt_required, otp_confirmed]

class ClusterNetworksList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('networks_infos_all')
    @api.marshal_with(networks_fields_get_many)
    def get(self, cluster_id):
        """
        Get networks list
        """
        current_identity = import_user()

        if current_identity.admin:
            networks = Network.query.filter(Network.cluster_id == cluster_id).all()
        else:
            networks = Network.query.filter(
                and_(Network.cluster_id == cluster_id,Network.maintenance_mode == False)).all()

        networks_list = []

        if not networks:
            api.abort(code=404, message='any Networks not found in this Cluster')

        for network in networks:
            networks_list.append(network.__jsonapi__())

        return {'data': networks_list}

    @user_has('networks_create')
    @api.expect(networks_fields_post, validate=True)
    @api.marshal_with(networks_fields_get)
    def post(self, cluster_id):
        """
        Create network
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']

           

            cluster = Cluster.query.get(cluster_id)
            if not cluster:
                api.abort(code=404, message='Network Type not found')

            network = Network(name=data['name'], cluster=cluster)

            if 'cloud_metadata' in data:
                network.cloud_metadata = data['cloud_metadata']
            if 'maintenance_mode' in data:
                network.maintenance_mode = data['maintenance_mode']
            if 'ipcost' in data:
                network.ipcost = data['ipcost']
            if 'portcost' in data:
                network.portcost = data['portcost']
            if 'tarfficcost' in data:
                network.tarfficcost = data['tarfficcost']
           

            db.session.add(network)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {'data': network.__jsonapi__()}, 201


class NetworksList(Resource):
    pass

class Networks(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('network_infos')
    @api.marshal_with(networks_fields_get)
    def get(self, id):
        """
        Get network
        """
        network = Network.query.get(id)

        if not network:
            api.abort(code=404, message='Network not found')

        current_identity = import_user()
        if not current_identity.admin:
            if network.maintenance_mode:
                api.abort(code=403, message='You do not have access. Network is on Maintenance Mode ')

        return {'data': network.__jsonapi__()}

    @user_has('networks_update')
    @api.expect(networks_fields_put, validate=True)
    @api.marshal_with(networks_fields_get)
    def put(self, id):
        """
        Update network
        """
        current_identity = import_user()
        if current_identity.admin:
            network = Network.query.get(id)

            if not network:
                api.abort(code=404, message='Network not found')

            data = request.get_json()['data']
            if 'name' in data:
                network.name = data['name']
            if 'cloud_metadata' in data:
                network.cloud_metadata = data['cloud_metadata']
            if 'maintenance_mode' in data:
                network.maintenance_mode = data['maintenance_mode']
            if 'ipcost' in data:
                network.ipcost = data['ipcost']
            if 'portcost' in data:
                network.portcost = data['portcost']
            if 'tarfficcost' in data:
                network.tarfficcost = data['tarfficcost']

        else:
            api.abort(code=403, message='You do not have access. ')

        if len(data) > 0:
            db.session.commit()

        return {'data': network.__jsonapi__()}

    @user_has('networks_delete')
    def delete(self, id):
        """
        Delete network
        """
        current_identity = import_user()
        if current_identity.admin:
            network = Network.query.get(id)

            if not network:
                api.abort(code=404, message='Network not found')

            db.session.delete(network)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204

class NetworkNetworkSubnetsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('network_subnets_infos_all')
    @api.marshal_with(network_subnets_fields_get_many)
    def get(self, network_id):
        """
        Get network_subnets list
        """
        current_identity = import_user()

        if current_identity.admin:
            network_subnets = NetworkSubnet.query.filter(NetworkSubnet.network_id == network_id).all()
        else:
            network_subnets = NetworkSubnet.query.filter(
                and_(NetworkSubnet.network_id == network_id, NetworkSubnet.maintenance_mode == False)).all()

        network_subnets_list = []

        if not network_subnets:
            api.abort(code=404, message='any NetworkSubnets not found in this Cluster')

        for network_subnet in network_subnets:
            network_subnets_list.append(network_subnet.__jsonapi__())

        return {'data': network_subnets_list}

    @user_has('network_subnets_create')
    @api.expect(network_subnets_fields_post, validate=True)
    @api.marshal_with(network_subnets_fields_get)
    def post(self, network_id):
        """
        Create network_subnet
        """
        current_identity = import_user()

        if current_identity.admin:
            data = request.get_json()['data']

            network = Network.query.get(network_id)
            if not network:
                api.abort(code=404, message='NetworkSubnet Type not found')

            network_subnet = NetworkSubnet(name=data['name'], network=network)

            if 'cloud_metadata' in data:
                network_subnet.cloud_metadata = data['cloud_metadata']
            if 'maintenance_mode' in data:
                network_subnet.maintenance_mode = data['maintenance_mode']
            if 'cidr' in data:
                network_subnet.cidr = data['cidr']
            if 'allocation_pools' in data:
                network_subnet.allocation_pools = data['allocation_pools']
            if 'dns_nameservers' in data:
                network_subnet.dns_nameservers = data['dns_nameservers']
            if 'enable_dhcp' in data:
                network_subnet.enable_dhcp = data['enable_dhcp']
            if 'gateway_ip' in data:
                network_subnet.gateway_ip = data['gateway_ip']
            if 'host_routes' in data:
                network_subnet.host_routes = data['host_routes']
            
                

            db.session.add(network_subnet)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {'data': network_subnet.__jsonapi__()}, 201

class NetworkSubnetsList(Resource):
    pass

class NetworkSubnets(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('network_subnet_infos')
    @api.marshal_with(network_subnets_fields_get)
    def get(self, id):
        """
        Get network_subnet
        """
        network_subnet = NetworkSubnet.query.get(id)

        if not network_subnet:
            api.abort(code=404, message='Network Subnet not found')

        current_identity = import_user()
        if not current_identity.admin:
            if network_subnet.maintenance_mode:
                api.abort(code=403, message='You do not have access. Network Subnet is on Maintenance Mode ')

        return {'data': network_subnet.__jsonapi__()}

    @user_has('network_subnets_update')
    @api.expect(network_subnets_fields_put, validate=True)
    @api.marshal_with(network_subnets_fields_get)
    def put(self, id):
        """
        Update network_subnet
        """
        current_identity = import_user()
        if current_identity.admin:
            network_subnet = NetworkSubnet.query.get(id)

            if not network_subnet:
                api.abort(code=404, message='NetworkSubnet not found')

            data = request.get_json()['data']
            if 'name' in data:
                network_subnet.name = data['name']
            if 'cloud_metadata' in data:
                network_subnet.cloud_metadata = data['cloud_metadata']
            if 'maintenance_mode' in data:
                network_subnet.maintenance_mode = data['maintenance_mode']
            if 'cidr' in data:
                network_subnet.cidr = data['cidr']
            if 'allocation_pools' in data:
                network_subnet.allocation_pools = data['allocation_pools']
            if 'dns_nameservers' in data:
                network_subnet.dns_nameservers = data['dns_nameservers']
            if 'enable_dhcp' in data:
                network_subnet.enable_dhcp = data['enable_dhcp']
            if 'gateway_ip' in data:
                network_subnet.gateway_ip = data['gateway_ip']
            if 'host_routes' in data:
                network_subnet.host_routes = data['host_routes']

        else:
            api.abort(code=403, message='You do not have access. ')

        if len(data) > 0:
            db.session.commit()

        return {'data': network_subnet.__jsonapi__()}

    @user_has('network_subnets_delete')
    def delete(self, id):
        """
        Delete network_subnet
        """
        current_identity = import_user()
        if current_identity.admin:
            network_subnet = NetworkSubnet.query.get(id)

            if not network_subnet:
                api.abort(code=404, message='NetworkSubnet not found')

            db.session.delete(network_subnet)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204

class SubnetsNetworkIpsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('network_ips_infos_all')
    @api.marshal_with(network_ips_fields_get_many)
    def get(self, network_subnet_id):
        """
        Get network_subnet network_ips list
        """


        network_ips = NetworkIp.query.filter(NetworkIp.network_subnet_id == network_subnet_id).all()


        network_ips_list = []

        if not network_ips:
            api.abort(code=404, message='any NetworkIps not found in this Cluster')

        for network_ip in network_ips:
            network_ips_list.append(network_ip.__jsonapi__())

        return {'data': network_ips_list}

    # @user_has('network_ips_create')
    # @api.expect(network_ips_fields_post, validate=True)
    # @api.marshal_with(network_ips_fields_get)
    # def post(self, network_subnet_id):
    #     """
    #     Create network_ip
    #     """
    #     current_identity = import_user()
    #
    #     if current_identity.admin:
    #         data = request.get_json()['data']
    #
    #         network_subnet = NetworkSubnet.query.get(network_subnet_id)
    #         if not network_subnet:
    #             api.abort(code=404, message='NetworkIp Type not found')
    #
    #         network_ip = NetworkIp(name=data['name'], network_subnet=network_subnet)
    #
    #         if 'cloud_metadata' in data:
    #             network_ip.cloud_metadata = data['cloud_metadata']
    #         if 'ip_address' in data:
    #             network_ip.ip_address = data['ip_address']
    #         if 'port_sharing' in data:
    #             network_ip.port_sharing = data['port_sharing']
    #
    #
    #         db.session.add(network_ip)
    #         db.session.commit()
    #     else:
    #         api.abort(code=403, message='You do not have access. ')
    #
    #     return {'data': network_ip.__jsonapi__()}, 201

class NetworkIpsList(Resource):
    pass

class NetworkIps(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('network_ip_infos')
    @api.marshal_with(network_ips_fields_get)
    def get(self, id):
        """
        Get network_ip
        """
        network_ip = NetworkIp.query.get(id)

        if not network_ip:
            api.abort(code=404, message='Network Subnet not found')

        # current_identity = import_user()
        # if not current_identity.admin:
        #     if network_ip.maintenance_mode:
        #         api.abort(code=403, message='You do not have access. Network Subnet is on Maintenance Mode ')

        return {'data': network_ip.__jsonapi__()}

    @user_has('network_ips_update')
    @api.expect(network_ips_fields_put, validate=True)
    @api.marshal_with(network_ips_fields_get)
    def put(self, id):
        """
        Update network_ip
        """
        current_identity = import_user()
        if current_identity.admin:
            network_ip = NetworkIp.query.get(id)

            if not network_ip:
                api.abort(code=404, message='NetworkIp not found')

            data = request.get_json()['data']
            if 'name' in data:
                network_ip.name = data['name']
            if 'cloud_metadata' in data:
                network_ip.cloud_metadata = data['cloud_metadata']
            if 'ip_address' in data:
                network_ip.ip_address = data['ip_address']
            if 'port_sharing' in data:
                network_ip.port_sharing = data['port_sharing']

        else:
            api.abort(code=403, message='You do not have access. ')

        if len(data) > 0:
            db.session.commit()

        return {'data': network_ip.__jsonapi__()}

    @user_has('network_ips_delete')
    def delete(self, id):
        """
        Delete network_ip
        """
        current_identity = import_user()
        if current_identity.admin:
            network_ip = NetworkIp.query.get(id)

            if not network_ip:
                api.abort(code=404, message='Network Ip not found')

            db.session.delete(network_ip)
            db.session.commit()
        else:
            api.abort(code=403, message='You do not have access. ')

        return {}, 204

class ContainerNetworkIpsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('network_ips_infos_all')
    @user_has_container("container_view")
    @api.marshal_with(network_ips_fields_get_many)
    def get(self, container_id):
        """
        Get container network_ips list
        """

        network_ips = NetworkIp.query.filter(NetworkIp.container_id== container_id).all()

        network_ips_list = []

        if not network_ips:
            api.abort(code=404, message='any Network Ips not found in this Cluster')

        for network_ip in network_ips:
            network_ips_list.append(network_ip.__jsonapi__())


        return {'data': network_ips_list}




    @user_has('containers_network_ip_create')
    @user_has_container("container_create")
    @api.expect(network_ips_fields_post, validate=False)
    @api.marshal_with(network_ips_fields_get)
    @api.doc(responses={
        201: 'Network IP created',
        409: 'Network IP already exists',
        500: 'Can\'t create Network IP'
    })
    def post(self, container_id):
            """
            Create container network_ip
            """
            current_identity = import_user()
            data = request.get_json()['data']
            if current_identity.admin:
                pass

            else:
                container=Container.query.get(container_id)
                if not container:
                    api.abort(code=404, message='Container not found')

                cluster_flavor_host_type=ClusterFlavorHostType.query.get(container.cluster_flavor_host_type_id)
                cluster=Cluster.query.get(cluster_flavor_host_type.cluster_id)

                network_ip = cluster.select_cluster_network_ip(
                                            project=container.project_id,
                                           container=container)
                if not network_ip:
                    api.abort(code=404, message='un assign Network IP is not Found. ')



                try:
                    cert=cluster.get_lxd_cluster_cert()
                    res = lgw.lxd_api_post(endpoint=cluster.endpoint,object='containers',cert=cert ,data=config)
                    # print(res.text)
                except Exception as e:
                    api.abort(code=500, message='Can\'t create container')

                if res.status_code == 202:

                    if 'cloud_metadata' in data:
                        network_ip.cloud_metadata = data['cloud_metadata']
                    db.session.add(network_ip)
                    db.session.commit()
                    # Get container ID
                    container = Container.query.filter_by(
                        name=data['name']).first()
                    # Add container to allowed users
                    if current_identity.admin:
                        try:
                            users_id = list(id['id'] for id in data['relationships']['users'])
                            for user_id in users_id:
                                user = User.query.get(user_id)
                                user.containers.append(container.id)
                                db.session.commit()
                        except KeyError:
                            pass
                        except AttributeError:
                            api.abort(code=500, message='User doesn\'t exists')
                    # Add container to current user
                    else:
                        user = User.query.get(current_identity.id)
                        user.containers.append(container.id)
                        db.session.commit()
                else:
                    api.abort(code=res.status_code, message='Error when creating container')

                # container_json = container.__jsonapi__()
                # return {'data': container_json}
                return res.json()
            api.abort(code=409, message='Container already exists')

class ProjectClusterNetworkIpsList(Resource):
    decorators = [jwt_required, otp_confirmed]

    @user_has('network_ips_infos_all')
    @user_has_project("container_view")
    @api.marshal_with(network_ips_fields_get_many)
    def get(self, project_id,cluster_id):
        """
        Get container network_ips list
        """
        network_ips=NetworkIp.query.join(Container).join(ClusterFlavorHostTypes).join(Cluster)\
            .filter(and_(NetworkIp.project_id == project_id,Cluster.id == cluster_id)).all()



        network_ips_list = []

        if not network_ips:
            api.abort(code=404, message='any Network Ips not found in this Cluster for this project.')

        for network_ip in network_ips:
            network_ips_list.append(network_ip.__jsonapi__())

        return {'data': network_ips_list}

    @user_has('projects_network_ip_create')
    @user_has_project("container_create")
    @api.expect(network_ips_fields_container_project_post, validate=False)
    # @api.marshal_with(containers_fields_get)
    @api.doc(responses={
        201: 'Network IP created',
        409: 'Network IP already exists',
        500: 'Can\'t create Network IP'
    })
    def post(self ,project_id,cluster_id):
        """
        Create container network_ip
        """
        current_identity = import_user()

        data = request.get_json()['data']
        if current_identity.admin:
            pass

        else:
            project = Project.query.get(project_id)
            if not project:
                api.abort(code=404, message='Project not found')

            cluster = Cluster.query.get(cluster_id)
            if not cluster:
                api.abort(code=404, message='Cluster not found')



            selected_ip,network_subnet_selected=cluster.select_cluster_network_ip()

            if not selected_ip:
                api.abort(code=404, message='un assign Network IP is not Found. ')

            try:
                res = lgw.lxd_api_post('containers', data=config)
                # print(res.text)
            except Exception as e:
                api.abort(code=500, message='Can\'t create container')

            if res.status_code == 202:
                # Add container to database
                network_ip = NetworkIp(ip_address=str(selected_ip),
                                       project=project,
                                       network_subnet=network_subnet_selected,
                                      )
                if 'cloud_metadata' in data:
                    network_ip.cloud_metadata = data['cloud_metadata']

                db.session.add(network_ip)
                db.session.commit()
                # Get container ID
                container = Container.query.filter_by(
                    name=data['name']).first()
                # Add container to allowed users
                if current_identity.admin:
                    try:
                        users_id = list(id['id'] for id in data['relationships']['users'])
                        for user_id in users_id:
                            user = User.query.get(user_id)
                            user.containers.append(container.id)
                            db.session.commit()
                    except KeyError:
                        pass
                    except AttributeError:
                        api.abort(code=500, message='User doesn\'t exists')
                # Add container to current user
                else:
                    user = User.query.get(current_identity.id)
                    user.containers.append(container.id)
                    db.session.commit()
            else:
                api.abort(code=res.status_code, message='Error when creating container')

            # container_json = container.__jsonapi__()
            # return {'data': container_json}
            return res.json()
        api.abort(code=409, message='Container already exists')


class ContainerNetworkIps(Resource):
    decorators = [jwt_required, otp_confirmed]

class HostNetworkIps(Resource):
    decorators = [jwt_required, otp_confirmed]

class ContainerNetworkProxysList(Resource):
    decorators = [jwt_required, otp_confirmed]

class ShareNetworkIpsNetworkProxysList(Resource):
    decorators = [jwt_required, otp_confirmed]

class NetworkProxysList(Resource):
    decorators = [jwt_required, otp_confirmed]

class NetworkProxys(Resource):
    decorators = [jwt_required, otp_confirmed]