#!/usr/bin/env python
# -*- coding: utf-8 -*-



if __name__ == '__main__':
    import create_db
    print('Creating database...')
    try:
        create_db._run()
        print('Database created!')
    except Exception as e:
        raise e

    import populate_db
    print('Populating database...')
    try:
        populate_db._run()
        print('Database populated!')
    except Exception as e:
        raise e
