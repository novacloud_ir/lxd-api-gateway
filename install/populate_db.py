#!/usr/bin/env python
# -*- coding: utf-8 -*-
from app.models import *
import sys
import os
sys.path.append(
    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))


def _run():
    ability = Ability(name='users_infos_all')  # 1 #admin
    db.session.add(ability)
    ability = Ability(name='users_create')  # 2 #admin
    db.session.add(ability)
    ability = Ability(name='users_infos')  # 3 #admin
    db.session.add(ability)
    ability = Ability(name='users_update')  # 4 #admin
    db.session.add(ability)
    ability = Ability(name='users_delete')  # 5 #admin
    db.session.add(ability)

    ability = Ability(name='groups_infos_all')  # 6 #admin
    db.session.add(ability)
    ability = Ability(name='groups_create')  # 7 #admin
    db.session.add(ability)
    ability = Ability(name='groups_infos')  # 8 #admin
    db.session.add(ability)
    ability = Ability(name='groups_update')  # 9 #admin
    db.session.add(ability)
    ability = Ability(name='groups_delete')  # 10 #admin
    db.session.add(ability)

    ability = Ability(name='abilities_infos_all')  # 11 #admin
    db.session.add(ability)
    ability = Ability(name='abilities_infos')  # 12 #admin
    db.session.add(ability)
    ability = Ability(name='abilities_update')  # 13 #admin
    db.session.add(ability)

    ability = Ability(name='me_infos')  # 14
    db.session.add(ability)
    ability = Ability(name='me_update')  # 15
    db.session.add(ability)
    ability = Ability(name='me_otp_create')  # 16
    db.session.add(ability)

    ability = Ability(name='requests_infos_all')  # 17
    db.session.add(ability)
    ability = Ability(name='requests_create')  # 18
    db.session.add(ability)
    ability = Ability(name='requests_infos')  # 19
    db.session.add(ability)
    ability = Ability(name='requests_update')  # 20
    db.session.add(ability)
    ability = Ability(name='requests_delete')  # 21 #admin
    db.session.add(ability)

    ability = Ability(name='containers_infos_all')  # 22
    db.session.add(ability)
    ability = Ability(name='containers_create')  # 23 #admin/user
    db.session.add(ability)
    ability = Ability(name='containers_infos')  # 24
    db.session.add(ability)
    ability = Ability(name='containers_update')  # 25 #admin/user
    db.session.add(ability)
    ability = Ability(name='containers_delete')  # 26 #admin/user
    db.session.add(ability)
    ability = Ability(name='containers_console')  # 27
    db.session.add(ability)
    ability = Ability(name='containers_state_infos')  # 28
    db.session.add(ability)
    ability = Ability(name='containers_state_update')  # 29
    db.session.add(ability)
    ability = Ability(name='containers_start')  # 30
    db.session.add(ability)
    ability = Ability(name='containers_freeze')  # 31
    db.session.add(ability)
    ability = Ability(name='containers_unfreeze')  # 32
    db.session.add(ability)
    ability = Ability(name='containers_stop')  # 33
    db.session.add(ability)
    ability = Ability(name='containers_stop_force')  # 34
    db.session.add(ability)
    ability = Ability(name='containers_restart')  # 35
    db.session.add(ability)

    ability = Ability(name='snapshots_infos_all')  # 36
    db.session.add(ability)
    ability = Ability(name='snapshots_create')  # 37
    db.session.add(ability)
    ability = Ability(name='snapshots_infos')  # 38
    db.session.add(ability)
    ability = Ability(name='snapshots_rename')  # 39
    db.session.add(ability)
    ability = Ability(name='snapshots_delete')  # 40
    db.session.add(ability)
    ability = Ability(name='snapshots_restore')  # 41 #admin/user
    db.session.add(ability)

    ability = Ability(name='images_infos_all')  # 42
    db.session.add(ability)
    ability = Ability(name='images_create')  # 43 #admin
    db.session.add(ability)
    ability = Ability(name='images_infos')  # 44
    db.session.add(ability)
    ability = Ability(name='images_update')  # 45 #admin
    db.session.add(ability)
    ability = Ability(name='images_delete')  # 46 #admin
    db.session.add(ability)
    ability = Ability(name='images_aliases_infos_all')  # 47
    db.session.add(ability)
    ability = Ability(name='images_aliases_create')  # 48 #admin
    db.session.add(ability)
    ability = Ability(name='images_aliases_infos')  # 49
    db.session.add(ability)
    ability = Ability(name='images_aliases_update')  # 50 #admin
    db.session.add(ability)
    ability = Ability(name='images_aliases_delete')  # 51 #admin
    db.session.add(ability)
    ability = Ability(name='images_remote_infos_all')  # 52 #admin
    db.session.add(ability)

    ability = Ability(name='operations_infos')  # 53
    db.session.add(ability)
    ability = Ability(name='lxd_infos')  # 54 #admin
    db.session.add(ability)
    ability = Ability(name='resources_infos')  # 55 #admin
    db.session.add(ability)
    ability = Ability(name='stats_infos')  # 56
    db.session.add(ability)
    ability = Ability(name='config_infos')  # 57
    db.session.add(ability)
    ability = Ability(name='config_update')  # 58 #admin
    db.session.add(ability)
    ability = Ability(name='lxd_certs_create')  # 59 #admin
    db.session.add(ability)

    ability = Ability(name='projects_infos_all')  # 60 #admin
    db.session.add(ability)
    ability = Ability(name='projects_create')  # 61 #admin
    db.session.add(ability)
    ability = Ability(name='project_infos')  # 62 #admin
    db.session.add(ability)
    ability = Ability(name='projects_update')  # 63 #admin
    db.session.add(ability)
    ability = Ability(name='projects_delete')  # 64 #admin
    db.session.add(ability)



    ability = Ability(name='all_users_projects_infos_all')  # 65 #admin
    db.session.add(ability)
    ability = Ability(name='users_projects_infos_all')  # 66 #admin
    db.session.add(ability)
    ability = Ability(name='users_roles_projects_infos_all')  # 67 #admin
    db.session.add(ability)
    ability = Ability(name='users_roles_projects_infos')  # 68 #admin
    db.session.add(ability)
    ability = Ability(name='users_roles_projects_update')  # 69 #admin
    db.session.add(ability)
    ability = Ability(name='users_roles_projects_delete')  # 70 #admin
    db.session.add(ability)

    ability = Ability(name='role_abilities_infos_all')  # 71 #admin
    db.session.add(ability)
    ability = Ability(name='role_abilities_create')  # 72 #admin
    db.session.add(ability)
    ability = Ability(name='roleAbility_infos')  # 73 #admin
    db.session.add(ability)
    ability = Ability(name='role_abilities_update')  # 74 #admin
    db.session.add(ability)
    ability = Ability(name='role_abilities_delete')  # 75 #admin
    db.session.add(ability)

    ability = Ability(name='roles_infos_all')  # 76 #admin
    db.session.add(ability)
    ability = Ability(name='roles_create')  # 77 #admin
    db.session.add(ability)
    ability = Ability(name='role_infos')  # 78 #admin
    db.session.add(ability)
    ability = Ability(name='roles_update')  # 79 #admin
    db.session.add(ability)
    ability = Ability(name='roles_delete')  # 80 #admin
    db.session.add(ability)

    datacenters_infos_all = Ability(name='datacenters_infos_all')  # 81 #admin
    db.session.add(datacenters_infos_all)

    datacenters_create = Ability(name='datacenters_create')  # 82 #admin
    db.session.add(datacenters_create)

    datacenter_infos = Ability(name='datacenter_infos')  # 83 #admin
    db.session.add(datacenter_infos)

    datacenters_update = Ability(name='datacenters_update')  # 84 #admin
    db.session.add(datacenters_update)

    datacenters_delete = Ability(name='datacenters_delete')  # 85 #admin
    db.session.add(datacenters_delete)

    clusters_infos_all = Ability(name='clusters_infos_all')  # 86 #admin
    db.session.add(clusters_infos_all)

    clusters_create = Ability(name='clusters_create')  # 87 #admin
    db.session.add(clusters_create)

    cluster_infos = Ability(name='cluster_infos')  # 88 #admin
    db.session.add(cluster_infos)

    clusters_update = Ability(name='clusters_update')  # 89 #admin
    db.session.add(clusters_update)

    clusters_delete = Ability(name='clusters_delete')  # 90 #admin
    db.session.add(clusters_delete)

    cluster_types_infos_all = Ability(name='cluster_types_infos_all')  # 91 #admin
    db.session.add(cluster_types_infos_all)

    cluster_types_create = Ability(name='cluster_types_create')  # 92 #admin
    db.session.add(cluster_types_create)

    cluster_type_infos = Ability(name='cluster_type_infos')  # 93 #admin
    db.session.add(cluster_type_infos)

    cluster_types_update = Ability(name='cluster_types_update')  # 94 #admin
    db.session.add(cluster_types_update)

    cluster_types_delete = Ability(name='cluster_types_delete')  # 95 #admin
    db.session.add(cluster_types_delete)

    host_types_infos_all = Ability(name='host_types_infos_all')  # 96 #admin
    db.session.add(host_types_infos_all)

    host_types_create = Ability(name='host_types_create')  # 97 #admin
    db.session.add(host_types_create)

    host_type_infos = Ability(name='host_type_infos')  # 98 #admin
    db.session.add(host_type_infos)

    host_types_update = Ability(name='host_types_update')  # 99 #admin
    db.session.add(host_types_update)

    host_types_delete = Ability(name='host_types_delete')  # 100 #admin
    db.session.add(host_types_delete)

    hosts_infos_all = Ability(name='hosts_infos_all')  # 101 #admin
    db.session.add(hosts_infos_all)

    hosts_create = Ability(name='hosts_create')  # 101 #admin
    db.session.add(hosts_create)

    host_infos = Ability(name='host_infos')  # 102 #admin
    db.session.add(host_infos)

    hosts_update = Ability(name='hosts_update')  # 103 #admin
    db.session.add(hosts_update)

    hosts_delete = Ability(name='hosts_delete')  # 104 #admin
    db.session.add(hosts_delete)

    flavors_infos_all = Ability(name='flavors_infos_all')  # 105 #admin
    db.session.add(flavors_infos_all)

    flavors_create = Ability(name='flavors_create')  # 106 #admin
    db.session.add(flavors_create)

    flavor_infos = Ability(name='flavor_infos')  # 107 #admin
    db.session.add(flavor_infos)

    flavors_update = Ability(name='flavors_update')  # 108 #admin
    db.session.add(flavors_update)

    networks_infos_all = Ability(name='networks_infos_all')  # 109 #admin
    db.session.add(networks_infos_all)

    networks_create = Ability(name='networks_create')  # 110 #admin
    db.session.add(networks_create)

    network_infos = Ability(name='network_infos')  # 111 #admin
    db.session.add(network_infos)

    networks_update = Ability(name='networks_update')  # 112 #admin
    db.session.add(networks_update)

    networks_delete = Ability(name='networks_delete')  # 113 #admin
    db.session.add(networks_delete)

    network_subnets_infos_all = Ability(name='network_subnets_infos_all')  # 114 #admin
    db.session.add(network_subnets_infos_all)

    network_subnets_create = Ability(name='network_subnets_create')  # 115 #admin
    db.session.add(network_subnets_create)

    network_subnet_infos = Ability(name='network_subnet_infos')  # 116 #admin
    db.session.add(network_subnet_infos)

    network_subnets_update = Ability(name='network_subnets_update')  # 117 #admin
    db.session.add(network_subnets_update)

    network_subnets_delete = Ability(name='network_subnets_delete')  # 118 #admin
    db.session.add(network_subnets_delete)

    network_ips_infos_all = Ability(name='network_ips_infos_all')  # 119 #admin
    db.session.add(network_ips_infos_all)

    network_ip_infos = Ability(name='network_ip_infos')  # 120 #admin
    db.session.add(network_ip_infos)

    network_ips_update = Ability(name='network_ips_update')  # 121 #admin
    db.session.add(network_ips_update)

    network_ips_delete = Ability(name='network_ips_delete')  # 122 #admin
    db.session.add(network_ips_delete)

    containers_network_ip_create = Ability(name='containers_network_ip_create')  # 123 #admin
    db.session.add(containers_network_ip_create)

    projects_network_ip_create = Ability(name='projects_network_ip_create')  # 124 #admin
    db.session.add(projects_network_ip_create)

    db.session.commit()

    group= Group(
        name='admin',
        abilities=[i for i in range(1, 124)]
    )

    db.session.add(group)
    db.session.commit()

    group2 = Group(
        name='user',
        abilities=[6, 11, 14, 15, 16, 17, 18, 19, 20, 22, 24, 27, 28, 29, 30,
                   31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 44, 47,
                   49, 53, 56, 57,60]
    )


    db.session.add(group2)
    db.session.commit()

    project_remove=RoleAbility("project_remove")
    db.session.add(project_remove)
    project_rename= RoleAbility("project_rename")
    db.session.add(project_rename)
    project_user_manage = RoleAbility("project_user_manage")
    db.session.add(project_user_manage)
    container_create = RoleAbility("container_create")
    db.session.add(container_create)
    container_modify = RoleAbility("container_modify")
    db.session.add(container_modify)
    container_delete = RoleAbility("container_delete")
    db.session.add(container_delete)
    container_maintain = RoleAbility("container_maintain")
    db.session.add(container_maintain)
    container_view = RoleAbility("container_view")
    db.session.add(container_view)

    db.session.commit()

    role_owner=Role("owner",role_abilities=[project_remove,project_rename,project_user_manage,container_create,container_modify
                                ,container_delete,container_maintain,container_view])
    db.session.add(role_owner)
    role_admin = Role("administrator",role_abilities=[container_create,container_modify
                                ,container_delete,container_maintain,container_view])
    db.session.add(role_admin)
    role_maintainer = Role("maintainer",role_abilities=[container_maintain,container_view])
    db.session.add(role_maintainer)
    role_viewer = Role("viewer",role_abilities=[container_view])
    db.session.add(role_viewer)

    db.session.commit()

    user = User(
        admin=True,
        name='John Doe',
        username='admin',
        groups=[1]
    )

    passwd = os.getenv('ADMIN_PASSWORD')
    if passwd:
        user.hash_password(passwd)
    else:
        user.hash_password('admin1234')
    db.session.add(user)
    db.session.commit()

tiny_flavor=Flavor(name="m1.tiny",cpu=1000,memory=1024,disk=5,traffic=100)
db.session.add(tiny_flavor)

xlhost_datacenter=DataCenter(name="xlhost",maintenance_mode=False)
db.session.add(xlhost_datacenter)

lxd_cluster_type= ClusterType(name='lxd',cluster_metadata_template={"cert_crt":" ","cert_key":" "})
db.session.add(lxd_cluster_type)

vm_host_type=HostType(name='vm')
db.session.add(vm_host_type)
vm_host_type=HostType(name='baremetal')
db.session.add(vm_host_type)


db.session.commit()


if __name__ == '__main__':
    _run()
