import requests




def _url(path):
    return 'http://209.190.27.14:5000' + path


def main():
    print('Get Token')
    resp = requests.post(_url('/api/v1/auth'), json={ "username": "admin",
                                                      "password": "admin1234"
                                                        })

    print('status_code={0}'.format(resp.status_code))
    tokens=resp.json()
    print('data={0}'.format(resp.json()))

    headers = {'Content-type': 'application/json',
               'Accept': 'application/json',
               'Authorization': 'Bearer {0}'.format(tokens['access_token'])}

    print('create_cluster xlhost_lxd')
    json={"data": {
    "name": "xlhost_lxd",
    "endpoint": "https://novacloud.ir:8443",
    "lxd_metadata": {
	"cert_crt":"-----BEGIN CERTIFICATE-----\nMIIFDzCCAvcCFFUvrl5zsgDZGaBTWwNFD4tlV+EEMA0GCSqGSIb3DQEBCwUAMEQx\nCzAJBgNVBAYTAklSMRMwEQYDVQQIDApTb21lLVN0YXRlMQ0wCwYDVQQKDAROb3Zh\nMREwDwYDVQQDDAhjbHVzdGVyMTAeFw0yMDA0MTUxOTUzMjhaFw0zMDA0MTMxOTUz\nMjhaMEQxCzAJBgNVBAYTAklSMRMwEQYDVQQIDApTb21lLVN0YXRlMQ0wCwYDVQQK\nDAROb3ZhMREwDwYDVQQDDAhjbHVzdGVyMTCCAiIwDQYJKoZIhvcNAQEBBQADggIP\nADCCAgoCggIBAMdG7gunk3vRcTRpysNuuBzMrUUqtdu6L1bloZFV547rXWoYJ8w0\nxiHUAzwVuxL03Zl9r/poD/4ZLOx2JzyLusEJMJ2D4E9jeKJbD0jpZMNJ8Z0k25uF\nhuax+sa107ffn/IffFjx45iNhXeH7ouLO23ikrIatBFLvD5cBn6OxyHYcH0xIivP\netDko57F67V7pt8Kj6ta2+NCy+izFeVhrhz7g16qVeN7VdgFUZ3uLl4SkOJcqxXa\n7Q5VBY/zh9Yk/72eQazl3oxNV4M/V4JBNh/3cgTHP24OSyvd9YQVvWooJFsm5H8H\nTw6xNsyVfXu08GjSL3g3ljwx+i/schgj29hwAPFZWqBMQq9dYRkRhKerf4bgKMkE\n3zdy3hWZP7pwYdKbTvHpXENESDgta3Nv9JEi0wsbZEp/tePQ32ED4VPkVrUJV6nO\nhBut+/AO09eESck0FLoOBlcWj6elOJ7luSbdNrrmLcI95g1vfYk0mf2XcNNBCBth\nGrcNiiVooGPMmFDP7ttSbMR16YAY/sYxwDTlnAGoyw7VlpqLJNJQDUj8x7B6IcaN\nx5LzK2XsGHUAXmx/yjQ8/rKxyKOb6HgcLZeEF16GDJvAOnwRq8s57E9/odkZr4JW\nQka463EWCkHZrmnS325EXQEWQKqO1IgXVHexVlaRNe6JEAzzqxHH7ELXAgMBAAEw\nDQYJKoZIhvcNAQELBQADggIBAKLmCDfgsNIr+gTnas/VM1+I1WgQkbBjrByxASN9\n/TD0qNYCqKgJ71zsrh3dsoUNtMyt50v/S1vycEXhl9cnudFQ1u8kIBcKgfhJ0FdR\n96gnLWEpy1dfidSjEpa8+sTIz9G4nr5LYDfqKvg+XgYmkOJTEt54X7Rm6Mu2APaS\nGNBX7UNEyOtAv1AY5Qxe9yWkqJPBu1O9cOlasr4LChzFFZqTwpp3n1S07iqwIK0d\nrn84Zk/CwTXNxXfwiFJw8KCRUBzKIGl2Mv+iatpe/OZ6/dB18h4M98EME1SyTgUq\nYavI0bWYSnW6MIE2LpeZ+REs+FdqOh3Q9w9yj2HkAFBnfX4YCjFBisUuPgjhC9Jn\nuyocOephvnfhEl5Fcp3SXPLbggf7ug2ilp5w1pI7ZzsRy6GFaK/aCZ3UUWjHC5kB\n4kSjpxkeCnDmtc7rrnkV3b/wQ1vnPr1c5nTYeuTHzTW77SnLM14DoUGaj4WxS7Br\nRjt1q7R1EZg6YSQ7XhP/enhrqQ020EhvxNTaO9hiZStoXVkfK2PLq09qqUAY73Wy\nb8Yj6E+lUPEks4KZMqK09UsfrN4/taBXM2ivpfP6f9BN7OlUPSIKMq2sf3E+l5um\nBqyakYcWXPZidacGkLaEUEOSXQJitST5I4yyUzOFw0vJcN7RdpSLo06F03jy15Cj\n9Z7b\n-----END CERTIFICATE-----\n",
    "cert_key":"-----BEGIN RSA PRIVATE KEY-----\nMIIJKwIBAAKCAgEAx0buC6eTe9FxNGnKw264HMytRSq127ovVuWhkVXnjutdahgn\nzDTGIdQDPBW7EvTdmX2v+mgP/hks7HYnPIu6wQkwnYPgT2N4olsPSOlkw0nxnSTb\nm4WG5rH6xrXTt9+f8h98WPHjmI2Fd4fui4s7beKSshq0EUu8PlwGfo7HIdhwfTEi\nK8960OSjnsXrtXum3wqPq1rb40LL6LMV5WGuHPuDXqpV43tV2AVRne4uXhKQ4lyr\nFdrtDlUFj/OH1iT/vZ5BrOXejE1Xgz9XgkE2H/dyBMc/bg5LK931hBW9aigkWybk\nfwdPDrE2zJV9e7TwaNIveDeWPDH6L+xyGCPb2HAA8VlaoExCr11hGRGEp6t/huAo\nyQTfN3LeFZk/unBh0ptO8elcQ0RIOC1rc2/0kSLTCxtkSn+149DfYQPhU+RWtQlX\nqc6EG6378A7T14RJyTQUug4GVxaPp6U4nuW5Jt02uuYtwj3mDW99iTSZ/Zdw00EI\nG2Eatw2KJWigY8yYUM/u21JsxHXpgBj+xjHANOWcAajLDtWWmosk0lANSPzHsHoh\nxo3HkvMrZewYdQBebH/KNDz+srHIo5voeBwtl4QXXoYMm8A6fBGryznsT3+h2Rmv\nglZCRrjrcRYKQdmuadLfbkRdARZAqo7UiBdUd7FWVpE17okQDPOrEcfsQtcCAwEA\nAQKCAgEAtZblBKnHs2S/i8t7gWs7SsMYgZbR8VBQIxdNi2qEf6/qh+tLGMJeaiUQ\nbYg/2J5Z8Kyf18dMC3hVf4SLbJjHrW/6ZQ7vMGisXQYtf3tu3qZbDZ+wnp+mtkIp\nCzpVm5Cj57bZURPbGgCcRrgPoAlYNmHo23OJ8tjYyNqC8ICa5TSeDPbg9Js0h3ZR\nwzUH/XCYN1cgZD1/cjBEL+BBa6z/RFnM9E6zxX92UktGfGu2xPKWJC/dlyTQL+c6\nElsQxQsiIGA96JfvnFo+DNODoqZZ8/dAZypJmrhbt7WjLKHC4SGDP6zJOxgl6nWC\nQhtXJYtUJ7CgoSPGO0Qvf8wui+DhS1TeVPC+ZTEzgnMQYDcaNkZwif1iSDEv+eD6\ncUHdlHVkqZZPEpAN3e5MlaYp4d+cTodtLvnf9xpQw55Xld+a9nqt6CSxgqKBtP29\nlE8uQk3NSgELczhp25LTCTzycioGVoU4+yIyqer7FmykLR5MOvyNOOCxeawzQgOD\nw+5QCLLV6ZPtZcFvWHSKltlijXaN4ZebGOnoWQjeo9iCl90+mVx+Clz6Txul6NRx\n12Z+1w8RTXDbwSTA1jHE8b5vGgJZEAtWrUlaO6YQJ+XO4aPDNtn2pb8NFkF9qGHq\nyrw03bwcNOR57eqmcmMCe+nmwZTsj1htQBhI3i8i1pd3SEWExUECggEBAPJAyX1B\nyhfjwBTalnKi/bpBQteIdJjD+OoPDwmVOBNgoLrc6yzdlX6G+eYZntQHnW0oLUUh\nMb4uKuV6Y3v1kT/gUec974gWYIMaQVI8wsorVUHEoTN6g/l0OIaQCRQZ0YE3nCOP\nKMbGZO69L6BPgNc/aKWT9OgWQbe75VCndC59fFsXWkrwNYJMFa+WqYo9Lemf8S1y\nLGd9S1nDXid9eH50yjLPNlXHEYiafWZIdRDncN5jnR59J15BlLt/979F1eQi77Of\nIIwngnPiTJgtN2zH7T6cW1GYt5ERK/+EagGsn+IPg82w2gQcxleDdAHp3aj6yi8G\n2tHLR+e84rn3JUsCggEBANKV1HZVlEKZffvvrIeEbnDJDJcY0BG+H+gF2eSMEFwB\nXuQlcgtDzkPFrzqq2x5RGLsAVR0DjAGXO1dNT1L36Iv1S35yCTODrlgaf3XDanoy\npw0BCFeO3BQYDk9FT0rhsFMMUlLms2a1wReqU3blUP0zNfEKNTqVFMUzNmE0v3UY\njDFBBHsRUJ+4Y9g8ogQ6DOS/dzzTMkKDbVck9WktVHDq7Uu58LqjYT0vOWtYdqsD\nf3HmOL3b3D9ZgF+HtNwe2wRwm0Qcu7U9rdY0dUImaKQPYsuHHB537b0VcLaj6Px8\nn5XfB2v9EDmo2fpU54VS2jVdd3DbefHEy+4AXERTPSUCggEBAOMLw4xIz88qHsms\nwNq6VlFtfucgth7DdZRw/qf9mavXoWzhRu0q1ETc3nRBSPHyHSLESAGFheh3vq6K\n0ZjJZBPY+wpeqxQB3Pyi69vWpL30dRiJ4Iet6OkwexXrAwc613djFkzhjNcX9Fsp\nJuz59ohGnp6pGB05VuiMCvVXrHUyGQ4Kyg54Kb5v7tc9vrKYemJ/C/yLCIEKFkgP\n6jvaxcbHUQbj3EnypEVCfgsSF936/HcCJ6z+oZoenvZIjntPSZNjBwNzPX7+PK69\nxwnNPygODRXkV1YXatxkmrcFdJETKFooLmDmwr6d9WrqAh0VAjGwszP2GboM6tyP\n1HN05hcCggEBAKh0FH4InU3dTQtgwUmypUnxefHQdnkK9qj7LT8Gz/tZWVf1oFHk\njrZmB3NNw3Rv+PF2JZ/M+402NdDzVfYPu+Vhdl+Sx0t/FdMCd11z0dQoersLTWoF\nLIWibKf60KAdA5ibbatFQtNxpyHQLW4ZqHyzH35O5R+UrAXHnsOJl3ILDfJEiVoN\nEsqBjNUoQ45QBYpiovZ0F2tfiPLB7NKS4U5mjyYZerlCAoIYKb12CY9pf+RT/qXZ\nn0jJxsgkXYgL4rWdgoas2Xb0/P8x/W7KHMDIUgZvcQ3qb7Hm2cAwRCrdYidYPq6i\nw/zr1lrK2l0rVsSDnQ0ZdGXzQBKGFUjOsUUCggEBAL4KQy2EZ8HOSecroS5G+dv9\n/6k4E4CW6m6/EAC4yabifdPFlxJ5X4oAczF91myl/43Y2caHZidSx/PnVDQNi1p5\nJQKuIZ3f/41mzzNJe+n9rUfguniyCJd8ls13tXvMGRwszsJU/JgjRYBPkqgUZrZy\nHUV/s+pZTEXcv6wDsv/O4ZeTQsxPyIUr5OxKzk6WfVyeLnGv7to16l0PjoA159B3\nCDjyRbjXIXZUIVnYouFK7WH/+Imw1ab2ojUM8Ml+2QQjZu77QSVhj0hkTqgz+xxB\n1KMwQmqyjLBPpMEozg/FxCVOgw80AJJ4QULSA/Kqm9qyVWxGUKSaY90Q1mu8/yw=\n-----END RSA PRIVATE KEY-----\n"},
    "maintenance_mode": False,
    "cpu_sum": 10,
    "memory_sum": 8192,
    "disk_sum": 40,
    "cpu_aloc_sum": 0,
    "memory_aloc_sum": 0,
    "disk_aloc_sum": 0,
    "type": "clusters"
          }
        }


    resp=requests.post(_url('/api/v1/lgw/datacenters/1/cluster_types/1/clusters'), json=json,headers=headers)
    print('status_code={0}'.format(resp.status_code))
    print('data={0}'.format(resp.json()))
    result_json ={'data': {'type': 'clusters',
                           'cluster_type': {'id': 1, 'name': 'lxd'},
                           'datacenter': {'id': 1, 'name': 'xlhost'},
                           'cluster_metadata':
                               {'cert_crt': '/root/lxd_cert/xlhost_lxd_None.crt',
                                'cert_key': '/root/lxd_cert/xlhost_lxd_None.key'},
                           'endpoint': 'https://novacloud.ir:8443',
                           'maintenance_mode': True,
                           'cpu_sum': 10,
                           'memory_sum': 8192,
                           'disk_sum': 40,
                           'cpu_aloc_sum': 0, 'memory_aloc_sum': 0, 'disk_aloc_sum': 0}}


    print('create hosts')
    print('create host novacloud-1')
    json={"data": {
            "maintenance_mode": True,
            "cpu": 2000,
            "memory": 2048,
            "disk": 0,
            "cpu_share": 3,
            "memory_share": 2,
            "cpu_aloc": 0,
            "memory_aloc": 0,
            "disk_aloc": 0,
            "name": "novacloud-1",
            "type": "hosts"
          }
        }

    resp = requests.post(_url('/api/v1/lgw/clusters/1/host_types/1/hosts'), json=json, headers=headers)
    print('status_code={0}'.format(resp.status_code))
    print('data={0}'.format(resp.json()))
    result_json = {'data': {'type': 'hosts',
                            'name': 'novacloud-1',
                            'host_type': {'id': 1, 'name': 'vm'},
                            'cluster': {'id': 1, 'name': 'xlhost_lxd'},
                            'maintenance_mode': True,
                            'cloud_metadata': None,
                            'cpu': 2000,
                            'memory': 2048,
                            'disk': 0,
                            'cpu_share': 3,
                            'memory_share': 2,
                            'cpu_aloc': 0, 'memory_aloc': None, 'disk_aloc': 0}}


    print('create host novacloud-2')
    json['data']["name"]="novacloud-2"

    resp = requests.post(_url('/api/v1/lgw/clusters/1/host_types/1/hosts'), json=json, headers=headers)
    print('status_code={0}'.format(resp.status_code))
    print('data={0}'.format(resp.json()))
    result_json = {'data': {'type': 'hosts',
                            'name': 'novacloud-2',
                            'host_type': {'id': 1, 'name': 'vm'},
                            'cluster': {'id': 1, 'name': 'xlhost_lxd'},
                            'maintenance_mode': True,
                            'cloud_metadata': None,
                            'cpu': 2000,
                            'memory': 2048,
                            'disk': 0,
                            'cpu_share': 3,
                            'memory_share': 2,
                            'cpu_aloc': 0, 'memory_aloc': None, 'disk_aloc': 0}}

    print('create host novacloud-3')
    json['data']["name"] = "novacloud-3"

    resp = requests.post(_url('/api/v1/lgw/clusters/1/host_types/1/hosts'), json=json, headers=headers)
    print('status_code={0}'.format(resp.status_code))
    print('data={0}'.format(resp.json()))
    result_json={'data': {'type': 'hosts',
                          'name': 'novacloud-3',
                          'host_type': {'id': 1, 'name': 'vm'},
                          'cluster': {'id': 1, 'name': 'xlhost_lxd'},
                          'maintenance_mode': True,
                          'cloud_metadata': None,
                          'cpu': 2000,
                          'memory': 2048,
                          'disk': 0,
                          'cpu_share': 3,
                          'memory_share': 2,
                          'cpu_aloc': 0, 'memory_aloc': None, 'disk_aloc': 0}}

    resp = requests.post(_url('/api/v1/lgw/clusters/1/host_types/1/hosts'), json=json, headers=headers)
    print('status_code={0}'.format(resp.status_code))
    if resp.status_code==201:
        print('data={0}'.format(resp.json()))





main()


# import unittest
#
#
# class MyTestCase(unittest.TestCase):
#     def test_something(self):
#         self.assertEqual(True, False)
#
#
#
#
# if __name__ == '__main__':
    # unittest.main()
